package br.gov.mt.indea.sizapi.webservice.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("VETERINARIO")
public class Veterinario extends Servidor implements Cloneable{
	
	private static final long serialVersionUID = -499425696054282002L;

    /*
     * Esse campo foi criado para armazenar o c�digo da veterinario que est� no campo id no webservice. 
     * Isto foi feito porque cada formIN deve referenciar uma c�pia da veterinario como ela era no momento da cria��o do formIN. 
     * E isto n�o seria poss�vel de manter na forma como a veterinario vem do webservice (com o id sendo chave natural)     
     */
    @Column(name="codigo_veterinario_svo")
    private Long codigoVeterinarioSVO;
	
	@Column(name = "numero_conselho")
	private String numeroConselho;
	
	@Column(name = "instituicao_formatura")
	private String instituicaoFormatura;
	
	@Column(name = "instituicao_trabalho")
	private String instituicaoTrabalho;
	
	@Column(name = "proprietario_instituicao")
	private String proprietarioInstituicao;
	
	@Column(name = "profissional_oficial")
	private String profissionalOficial;
	
	private String pesquisador;
	
	@Column(name = "data_formatura")
	private Date dataFormatura;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_conselho")
    private Conselho conselho;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_tipo_emitente")
    private TipoEmitente tipoEmitente;

	public String getProfissionalOficialPorExtenso(){
		if (this.profissionalOficial == null)
			return null;
		
		if (this.profissionalOficial.equals("N"))
			return "N�O";
		else
			return "SIM";
	}

	public String getNumeroConselho() {
		return numeroConselho;
	}

	public void setNumeroConselho(String numeroConselho) {
		this.numeroConselho = numeroConselho;
	}

	public String getInstituicaoFormatura() {
		return instituicaoFormatura;
	}

	public void setInstituicaoFormatura(String instituicaoFormatura) {
		this.instituicaoFormatura = instituicaoFormatura;
	}

	public String getInstituicaoTrabalho() {
		return instituicaoTrabalho;
	}

	public void setInstituicaoTrabalho(String instituicaoTrabalho) {
		this.instituicaoTrabalho = instituicaoTrabalho;
	}

	public String getProprietarioInstituicao() {
		return proprietarioInstituicao;
	}

	public void setProprietarioInstituicao(String proprietarioInstituicao) {
		this.proprietarioInstituicao = proprietarioInstituicao;
	}

	public String getProfissionalOficial() {
		return profissionalOficial;
	}

	public void setProfissionalOficial(String profissionalOficial) {
		this.profissionalOficial = profissionalOficial;
	}

	public String getPesquisador() {
		return pesquisador;
	}

	public void setPesquisador(String pesquisador) {
		this.pesquisador = pesquisador;
	}

	public Date getDataFormatura() {
		return dataFormatura;
	}

	public void setDataFormatura(Date dataFormatura) {
		this.dataFormatura = dataFormatura;
	}

	public Conselho getConselho() {
		return conselho;
	}

	public void setConselho(Conselho conselho) {
		this.conselho = conselho;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TipoEmitente getTipoEmitente() {
		return tipoEmitente;
	}

	public void setTipoEmitente(TipoEmitente tipoEmitente) {
		this.tipoEmitente = tipoEmitente;
	}

	public Long getCodigoVeterinarioSVO() {
		return codigoVeterinarioSVO;
	}

	public void setCodigoVeterinarioSVO(Long codigoVeterinarioSVO) {
		this.codigoVeterinarioSVO = codigoVeterinarioSVO;
	}
	
	public Object clone() throws CloneNotSupportedException {
		Veterinario cloned = (Veterinario) super.clone();
		
		cloned.setId(null);
		
		if (this.getConselho() != null)
			cloned.setConselho((Conselho) this.getConselho().clone());
		if (this.getEndereco() != null)
			cloned.setEndereco((Endereco) this.getEndereco().clone());
		if (this.getTipoEmitente() != null)
			cloned.setTipoEmitente((TipoEmitente) this.getTipoEmitente().clone());
		
		return cloned;
	}
	
}