package br.gov.mt.indea.sizapi.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sizapi.enums.Dominio.SimNao;

@Entity(name="chave_principal_visita")
public class ChavePrincipalVisitaPropriedadeRural extends BaseEntity<Long>{

	private static final long serialVersionUID = -9209989053563785551L;
	
	@Id
	@SequenceGenerator(name="chave_principal_visita_seq", sequenceName="chave_principal_visita_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="chave_principal_visita_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "motivo_principal")
	private SimNao motivoPrincipal = SimNao.NAO;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_tipo_chave_principal_visit")
	private TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural;
	
	@JsonIgnore
	@OneToMany(mappedBy="chavePrincipalVisitaPropriedadeRural", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<ChaveSecundariaVisitaPropriedadeRural> listChaveSecundariaVisitaPropriedadeRural = new ArrayList<ChaveSecundariaVisitaPropriedadeRural>();
	
	@ManyToOne
	@JoinColumn(name="id_principal_visita")
	private VisitaPropriedadeRural visitaPropriedadeRural;
	
	@Column(name="quantidade_apreensoes")
	private Integer quantidadeApreensoes;
	
	@Column(name="quantidade_destruicoes")
	private Integer quantidadeDestruicoes;
	
	public String getListaChaveSecundariaVisitaPropriedadeRuralAsString(){
		StringBuilder sb = new StringBuilder();
		
		if (this.listChaveSecundariaVisitaPropriedadeRural != null && !this.listChaveSecundariaVisitaPropriedadeRural.isEmpty()){
			for (ChaveSecundariaVisitaPropriedadeRural chaveSecundariaVisitaPropriedadeRural : this.listChaveSecundariaVisitaPropriedadeRural) {
				sb.append(chaveSecundariaVisitaPropriedadeRural.getTipoChaveSecundariaVisitaPropriedadeRural().getNome()).append(", ");
			}
			
			sb = new StringBuilder(sb.toString().substring(0, sb.length()-2));
		}
		
		return sb.toString();
	}
	
	public boolean isChaveMotivoPrincipal(){
		return this.motivoPrincipal.equals(SimNao.SIM);
	}
	
	public boolean isChavePrincipalAtiva(){
		return this.tipoChavePrincipalVisitaPropriedadeRural.getStatus().equals(AtivoInativo.ATIVO);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoChavePrincipalVisitaPropriedadeRural getTipoChavePrincipalVisitaPropriedadeRural() {
		return tipoChavePrincipalVisitaPropriedadeRural;
	}

	public void setTipoChavePrincipalVisitaPropriedadeRural(
			TipoChavePrincipalVisitaPropriedadeRural tipoChavePrincipalVisitaPropriedadeRural) {
		this.tipoChavePrincipalVisitaPropriedadeRural = tipoChavePrincipalVisitaPropriedadeRural;
	}

	public List<ChaveSecundariaVisitaPropriedadeRural> getListChaveSecundariaVisitaPropriedadeRural() {
		return listChaveSecundariaVisitaPropriedadeRural;
	}

	public void setListChaveSecundariaVisitaPropriedadeRural(
			List<ChaveSecundariaVisitaPropriedadeRural> listChaveSecundariaVisitaPropriedadeRural) {
		this.listChaveSecundariaVisitaPropriedadeRural = listChaveSecundariaVisitaPropriedadeRural;
	}

	public VisitaPropriedadeRural getVisitaPropriedadeRural() {
		return visitaPropriedadeRural;
	}

	public void setVisitaPropriedadeRural(
			VisitaPropriedadeRural visitaPropriedadeRural) {
		this.visitaPropriedadeRural = visitaPropriedadeRural;
	}

	public Integer getQuantidadeApreensoes() {
		return quantidadeApreensoes;
	}

	public void setQuantidadeApreensoes(Integer quantidadeApreensoes) {
		this.quantidadeApreensoes = quantidadeApreensoes;
	}

	public Integer getQuantidadeDestruicoes() {
		return quantidadeDestruicoes;
	}

	public void setQuantidadeDestruicoes(Integer quantidadeDestruicoes) {
		this.quantidadeDestruicoes = quantidadeDestruicoes;
	}

	public SimNao getMotivoPrincipal() {
		return motivoPrincipal;
	}

	public void setMotivoPrincipal(SimNao motivoPrincipal) {
		this.motivoPrincipal = motivoPrincipal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChavePrincipalVisitaPropriedadeRural other = (ChavePrincipalVisitaPropriedadeRural) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}