package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name = "tipo_logradouro")
public class TipoLogradouro extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = -2546340099507509086L;

	@Id
	@SequenceGenerator(name = "tipo_logradouro_seq", sequenceName = "tipo_logradouro_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_logradouro_seq")
    private Long id;
     
    @Column(name = "nome")
	private String nome;

 	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	protected Object clone() throws CloneNotSupportedException {
		TipoLogradouro cloned = (TipoLogradouro) super.clone();
		
		cloned.setId(null);
		
		return cloned;
	}
	
}