package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import br.gov.mt.indea.sizapi.entity.Pessoa;

@Entity
@DiscriminatorValue("PRODUTOR")
public class Produtor extends Pessoa implements Cloneable {

    private static final long serialVersionUID = 8654661522116410848L;
    
    /*
     * Esse campo foi criado para armazenar o c�digo da produtor que est� no campo id no webservice. 
     * Isto foi feito porque cada formIN deve referenciar uma c�pia da produtor como ela era no momento da cria��o do formIN. 
     * E isto n�o seria poss�vel de manter na forma como a produtor vem do webservice (com o id sendo chave natural)     
     */
    @Column(name="codigo_produtor")
    private Long codigoProdutor;
    
	@Column(name = "assoc_sindicato_rural")
    private String assSindicatoRural;
    
    @Column(name = "coop_cooperativa_rural")
    private String coopRural;
    
    @Column(name = "tv_preferencia")
    private String tvPreferencia;
    
    @Column(name = "jornal_preferencia")
    private String jornalPreferencia;
    
    @Column(name = "radio_preferencia")
    private String radioPreferencia;

    public Produtor() {
    }
    
    public String getEnderecoAsString(){
    	StringBuilder sb = new StringBuilder();
    	
    	if (this.getEndereco() != null){
    		if (this.getEndereco().getTipoLogradouro() != null){
    			sb.append(this.getEndereco().getTipoLogradouro().getNome())
    				.append(this.getEndereco().getLogradouro());
    			
    			if (this.getEndereco().getNumero() != null)
    				sb.append(", n�")
    					.append(this.getEndereco().getNumero());
    			
    			if (this.getEndereco().getComplemento() != null)
    				sb.append(", ")
    					.append(this.getEndereco().getComplemento());
    			
    			if (this.getEndereco().getBairro() != null)
    				sb.append(". ")
    					.append(this.getEndereco().getBairro());
    		}
    	}
    	
    	return sb.toString();
    }

    public String getAssSindicatoRural() {
        return assSindicatoRural;
    }

    public void setAssSindicatoRural(String assSindicatoRural) {
        this.assSindicatoRural = assSindicatoRural;
    }

    public String getCoopRural() {
        return coopRural;
    }

    public void setCoopRural(String coopRural) {
        this.coopRural = coopRural;
    }

    public String getTvPreferencia() {
        return tvPreferencia;
    }

    public void setTvPreferencia(String tvPreferencia) {
        this.tvPreferencia = tvPreferencia;
    }

    public String getJornalPreferencia() {
        return jornalPreferencia;
    }

    public void setJornalPreferencia(String jornalPreferencia) {
        this.jornalPreferencia = jornalPreferencia;
    }

    public String getRadioPreferencia() {
        return radioPreferencia;
    }

    public void setRadioPreferencia(String radioPreferencia) {
        this.radioPreferencia = radioPreferencia;
    }

	public Long getCodigoProdutor() {
		return codigoProdutor;
	}

	public void setCodigoProdutor(Long codigoProdutor) {
		this.codigoProdutor = codigoProdutor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoProdutor == null) ? 0 : codigoProdutor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produtor other = (Produtor) obj;
		if (codigoProdutor == null) {
			if (other.codigoProdutor != null)
				return false;
		} else if (!codigoProdutor.equals(other.codigoProdutor))
			return false;
		return true;
	}
	
	public Object clone() throws CloneNotSupportedException{
		Produtor cloned = (Produtor) super.clone();
		
		cloned.setId(null);
		
		if (this.getEndereco() != null)
			cloned.setEndereco((Endereco) this.getEndereco().clone());
		
		return cloned;
	}

}