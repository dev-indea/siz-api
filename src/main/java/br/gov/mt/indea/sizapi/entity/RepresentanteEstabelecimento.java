package br.gov.mt.indea.sizapi.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import br.gov.mt.indea.sizapi.enums.Dominio.FuncaoNoEstabelecimento;

@Entity
@DiscriminatorValue("REPRESENTANTE_ESTABELECIMENTO")
public class RepresentanteEstabelecimento extends Pessoa implements Cloneable {

	private static final long serialVersionUID = -4755058620808121405L;
	
	@ElementCollection(targetClass = FuncaoNoEstabelecimento.class)
	@JoinTable(name = "funcao_do_rep_no_estab", joinColumns = @JoinColumn(name = "id_representante_estabelec", nullable = false))
	@Column(name = "id_funcao_do_rep_no_estab")
	@Enumerated(EnumType.STRING)
	private List<FuncaoNoEstabelecimento> funcoesNoEstabelecimento = new ArrayList<FuncaoNoEstabelecimento>();

	public List<FuncaoNoEstabelecimento> getFuncoesNoEstabelecimento() {
		return funcoesNoEstabelecimento;
	}

	public void setFuncoesNoEstabelecimento(
			List<FuncaoNoEstabelecimento> funcoesNoEstabelecimento) {
		this.funcoesNoEstabelecimento = funcoesNoEstabelecimento;
	}

	protected Object clone() throws CloneNotSupportedException {
		RepresentanteEstabelecimento cloned = (RepresentanteEstabelecimento) super.clone();
		
		cloned.setId(null);
		
		if (this.funcoesNoEstabelecimento != null){
			cloned.setFuncoesNoEstabelecimento(new ArrayList<FuncaoNoEstabelecimento>());
			for (FuncaoNoEstabelecimento funcao : this.funcoesNoEstabelecimento) {
				cloned.getFuncoesNoEstabelecimento().add(funcao);
			}
		}
		
		return cloned;
	}
	
}
