package br.gov.mt.indea.sizapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="numero_visita_prop_rural")
public class NumeroVisitaPropriedadeRural extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -3513472125122553076L;

	@Id
	@SequenceGenerator(name="numero_visita_prop_rural_seq", sequenceName="numero_visita_prop_rural_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="numero_visita_prop_rural_seq")
	private Long id;
	
	@OneToOne
	@JoinColumn(name="id_municipio")
	private Municipio municipio;
	
	@Column(name="ultimo_numero")
	private Long ultimoNumero;
	
	@Column(name="ano")
	private Long ano;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getUltimoNumero() {
		return ultimoNumero;
	}

	public void setUltimoNumero(Long ultimoNumero) {
		this.ultimoNumero = ultimoNumero;
	}

	public Long getAno() {
		return ano;
	}

	public void setAno(Long ano) {
		this.ano = ano;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result
				+ ((ultimoNumero == null) ? 0 : ultimoNumero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumeroVisitaPropriedadeRural other = (NumeroVisitaPropriedadeRural) obj;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (ultimoNumero == null) {
			if (other.ultimoNumero != null)
				return false;
		} else if (!ultimoNumero.equals(other.ultimoNumero))
			return false;
		return true;
	}

	public void incrementNumeroVisitaPropriedadeRural() {
		this.ultimoNumero++;
	}

}