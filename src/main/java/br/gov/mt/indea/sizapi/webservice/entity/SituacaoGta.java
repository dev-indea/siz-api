package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name = "situacao_gta")
public class SituacaoGta extends BaseEntity<String> {

    private static final long serialVersionUID = 6786863644588098137L;

	@Id
    @SequenceGenerator(name = "situacao_gta_seq", sequenceName = "situacao_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "situacao_gta_seq")
    private Long idTable;
	
	@Column(name = "identificador_situacao_gta")
    private String id;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "codigo_pga")
    private String codigoPga;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
	
}