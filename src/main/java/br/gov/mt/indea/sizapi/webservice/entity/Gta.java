package br.gov.mt.indea.sizapi.webservice.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Pessoa;
import br.gov.mt.indea.sizapi.entity.UF;

@Entity
public class Gta extends BaseEntity<Long> implements Comparable<Gta> {

	private static final long serialVersionUID = -3669361705087669220L;
	
	@Id
    @SequenceGenerator(name = "gta_seq", sequenceName = "gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gta_seq")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pessoa")
    private Pessoa emitente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_finalidade_gta")
    private FinalidadeGta finalidade;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_emitente")
    private TipoEmitente tipoEmitente;
   
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_grupo_especie")
    private GrupoEspecie grupoEspecie;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_especie")
    private Especie especie;
    
    @ManyToOne
    @JoinColumn(name = "id_uf_origem")
    private UF ufOrigem;
    
    @ManyToOne
    @JoinColumn(name = "id_uf_destino")
    private UF ufDestino;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_gta")
    private TipoGta tipoGta;
   
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval=true)
    @JoinColumn(name = "id_pessoa_destino")
    private PessoaDestinoGta pessoaDestino;
    
    @OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_pessoa_origem")
    private PessoaOrigemGta pessoaOrigemGta;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_situacao_gta")
    private SituacaoGta situacaoGta;
    
    @Column(name = "numero")
    private Integer numero;
    
    @Column(name = "serie")
    private String serie;
    
    @Column(name = "data_emissao")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataEmissao;
    
    @Column(name = "data_validade")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataValidade;
    
    @Column(name = "data_saida")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataSaida;
    
    @Column(name = "data_chegada")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataChegada;
    
    @Column(name = "data_digitacao_entrada")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataDigitacaoEntrada;
    
    @Column(name = "data_cancelamento")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataCancelamento;
    
    @Column(name = "data_atualizacao")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAtualizacao;
    
    @OneToMany(mappedBy="gta", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
    private List<EstratificacaoGta> estratificacoes;
    
    @Transient
    private boolean selecionadoParaInclusao;
    
    public Long getQuantidadeTotalAnimais(){
    	
    	if (estratificacoes != null){
    		Long qtdade = new Long(0);
    		
    		for (EstratificacaoGta estratificacaoGta : estratificacoes) {
				qtdade += estratificacaoGta.getQntRecebida();
			}
    		
    		return qtdade;
    	}
    	
    	throw new IllegalStateException();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getEmitente() {
        return emitente;
    }

    public void setEmitente(Pessoa emitente) {
        this.emitente = emitente;
    }

    public FinalidadeGta getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(FinalidadeGta finalidade) {
        this.finalidade = finalidade;
    }

    public GrupoEspecie getGrupoEspecie() {
        return grupoEspecie;
    }

    public void setGrupoEspecie(GrupoEspecie grupoEspecie) {
        this.grupoEspecie = grupoEspecie;
    }

    public Especie getEspecie() {
        return especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }
    
    public void setEspecie(String especie) {
    	Especie e = new Especie();
    	e.setEsp_nome(especie);
    	this.setEspecie(e);
    }

    public UF getUfOrigem() {
        return ufOrigem;
    }

    public void setUfOrigem(UF ufOrigem) {
        this.ufOrigem = ufOrigem;
    }

    public UF getUfDestino() {
        return ufDestino;
    }

    public void setUfDestino(UF ufDestino) {
        this.ufDestino = ufDestino;
    }

    public TipoGta getTipoGta() {
        return tipoGta;
    }

    public void setTipoGta(TipoGta tipoGta) {
        this.tipoGta = tipoGta;
    }

    public SituacaoGta getSituacaoGta() {
        return situacaoGta;
    }

    public void setSituacaoGta(SituacaoGta situacaoGta) {
        this.situacaoGta = situacaoGta;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public TipoEmitente getTipoEmitente() {
		return tipoEmitente;
	}

	public void setTipoEmitente(TipoEmitente tipoEmitente) {
		this.tipoEmitente = tipoEmitente;
	}

	public PessoaDestinoGta getPessoaDestino() {
		return pessoaDestino;
	}

	public void setPessoaDestino(PessoaDestinoGta pessoaDestino) {
		this.pessoaDestino = pessoaDestino;
	}

	public PessoaOrigemGta getPessoaOrigemGta() {
		return pessoaOrigemGta;
	}

	public void setPessoaOrigemGta(PessoaOrigemGta pessoaOrigemGta) {
		this.pessoaOrigemGta = pessoaOrigemGta;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataChegada() {
		return dataChegada;
	}

	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}

	public Date getDataDigitacaoEntrada() {
		return dataDigitacaoEntrada;
	}

	public void setDataDigitacaoEntrada(Date dataDigitacaoEntrada) {
		this.dataDigitacaoEntrada = dataDigitacaoEntrada;
	}

	public Date getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public boolean isSelecionadoParaInclusao() {
		return selecionadoParaInclusao;
	}

	public void setSelecionadoParaInclusao(boolean selecionadoParaInclusao) {
		this.selecionadoParaInclusao = selecionadoParaInclusao;
	}

	public List<EstratificacaoGta> getEstratificacoes() {
		return estratificacoes;
	}

	public void setEstratificacoes(List<EstratificacaoGta> estratificacoes) {
		this.estratificacoes = estratificacoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((serie == null) ? 0 : serie.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gta other = (Gta) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (serie == null) {
			if (other.serie != null)
				return false;
		} else if (!serie.equals(other.serie))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Gta o) {
		if (this.getNumero() < o.getNumero())
			return -1;
		else if (this.getNumero() == o.getNumero())
			return 0;
		else
			return 1;
	}

}