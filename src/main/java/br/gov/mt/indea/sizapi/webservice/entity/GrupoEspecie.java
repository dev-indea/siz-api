package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name="grupo_especie")
public class GrupoEspecie extends BaseEntity<String> {

    private static final long serialVersionUID = -1852035611975703831L;

	@Id
    @SequenceGenerator(name = "grupo_especie_seq", sequenceName = "grupo_especie_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "grupo_especie_seq")
	private Long idTable;
    
	@Column(name = "identificacao_grupo_especie")
	private String id;
	
	@Column(name = "nome")
    private String nome;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}

}