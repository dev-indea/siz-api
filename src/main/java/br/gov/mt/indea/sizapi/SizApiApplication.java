package br.gov.mt.indea.sizapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SizApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SizApiApplication.class, args);
	}

}
