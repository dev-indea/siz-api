package br.gov.mt.indea.sizapi.webservice.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;

@Entity
public class Abatedouro extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 7828915113434982873L;

	@Id
    @SequenceGenerator(name = "abatedouro_seq", sequenceName = "abatedouro_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "abatedouro_seq")
	private Long id;
    
	@Column(length=300)
    private String nome;
    
	@Column(length=300)
    private String apelido;
    
    private String cnpj;
    
    private String codigo;
    
    @JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    @JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;
    
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_coordenada_geografica")
    private CoordenadaGeografica coordenadaGeografica;
    
	@Column(name="")
    private Long numeroInspecao;
    
	@Column(name="")
    private String tipoInspecao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Long getNumeroInspecao() {
		return numeroInspecao;
	}

	public void setNumeroInspecao(Long numeroInspecao) {
		this.numeroInspecao = numeroInspecao;
	}

	public String getTipoInspecao() {
		return tipoInspecao;
	}

	public void setTipoInspecao(String tipoInspecao) {
		this.tipoInspecao = tipoInspecao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public CoordenadaGeografica getCoordenadaGeografica() {
		return coordenadaGeografica;
	}

	public void setCoordenadaGeografica(CoordenadaGeografica coordenadaGeografica) {
		this.coordenadaGeografica = coordenadaGeografica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Abatedouro other = (Abatedouro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}