package br.gov.mt.indea.sizapi.sevice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.gov.mt.indea.sizapi.entity.VisitaPropriedadeRural;
import br.gov.mt.indea.sizapi.repository.VisitaPropriedadeRuralRepository;

@RestController
public class VisitaPropriedadeRuralService {
	
	@Autowired
	private VisitaPropriedadeRuralRepository visitaPropriedadeRuralRepository;
	
	@GetMapping("visitaPropriedadeRural/{id}")
	public VisitaPropriedadeRural getVisitaPropriedadeRuralById(@PathVariable Long id) {
		return visitaPropriedadeRuralRepository.findById(id).get();
	}

}
