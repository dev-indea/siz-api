package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;
import br.gov.mt.indea.sizapi.entity.Pessoa;

@Entity
public class Recinto extends BaseEntity<Long>  {

	private static final long serialVersionUID = 9165858847065283683L;

	@Id
    @SequenceGenerator(name = "recinto_seq", sequenceName = "recinto_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recinto_seq")
	private Long id;
    
	/*
     * Esse campo foi criado para armazenar o c�digo do recinto que est� no campo id no webservice. 
     * Isto foi feito porque cada formIN deve referenciar uma c�pia do recinto como ela era no momento da cria��o do formIN. 
     * E isto n�o seria poss�vel de manter na forma como o recinto vem do webservice (com o id sendo chave natural)     
     */
    @Column(name="codigo_recinto")
    private Long codigoRecinto;
    
    @Column(name = "tipo_pessoa")
    private String tipoPessoa;
    
    @Column(name = "cnpj_cpf")
    private String cpfCnpj;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "apelido")
    private String apelido;
    
    @Column(name = "email_contato")
    private String email;
    
    @JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;
	
    @JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_pessoa")
    private Pessoa responsavel;
    
    @JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_veterinario")
    private Veterinario responsavelTecnico;
    
    @JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    @JsonIgnore
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_unidade")
    private ULE ule;
    
    @Column(name = "endereco_recinto")
    private String enderecoRecinto;
    
    @Column(name = "via_acesso")
    private String viaAcesso;
    
    @Column(name = "observacao")
    private String observacao;

    public Pessoa getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Pessoa responsavel) {
        this.responsavel = responsavel;
    }

    public Veterinario getResponsavelTecnico() {
        return responsavelTecnico;
    }

    public void setResponsavelTecnico(Veterinario responsavelTecnico) {
        this.responsavelTecnico = responsavelTecnico;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public ULE getUle() {
        return ule;
    }

    public void setUle(ULE ule) {
        this.ule = ule;
    }

    public String getEnderecoRecinto() {
        return enderecoRecinto;
    }

    public void setEnderecoRecinto(String enderecoRecinto) {
        this.enderecoRecinto = enderecoRecinto;
    }

    public String getViaAcesso() {
        return viaAcesso;
    }

    public void setViaAcesso(String viaAcesso) {
        this.viaAcesso = viaAcesso;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Long getCodigoRecinto() {
		return codigoRecinto;
	}

	public void setCodigoRecinto(Long codigoRecinto) {
		this.codigoRecinto = codigoRecinto;
	}

}