package br.gov.mt.indea.sizapi.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="uf")
public class UF extends BaseEntity<Long>{
	
	private static final long serialVersionUID = 957986718425855043L;
	
	public static final UF MATO_GROSSO = new UF(25L);
	
	public UF(Long id){
		this.id = id;
	}
	
	public UF(){}

	@Id
	@SequenceGenerator(name="uf_seq", sequenceName="uf_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="uf_seq")
	private Long id;
	
	@Column(name="codg_ibge", length=2)
	private String codgIBGE;
	
	@Column(length=2)
	private String uf;
	
	@Column(length=140)
	private String nome;
	
	@JsonIgnore
	@OneToMany(mappedBy="uf", cascade={CascadeType.PERSIST}, fetch=FetchType.LAZY)
	private List<Municipio> municipios;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_pais", nullable=false)
	private Pais pais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodgIBGE() {
		return codgIBGE;
	}

	public void setCodgIBGE(String codIBGE) {
		this.codgIBGE = codIBGE;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}
	
	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UF other = (UF) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}