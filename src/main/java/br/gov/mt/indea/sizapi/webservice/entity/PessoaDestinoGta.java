package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;
import br.gov.mt.indea.sizapi.entity.Pessoa;

@Entity(name = "pessoa_destino_gta")
public class PessoaDestinoGta extends BaseEntity<Long> {

	private static final long serialVersionUID = 1452568790403413774L;
	
	@Id
    @SequenceGenerator(name = "pessoa_destino_gta_seq", sequenceName = "pessoa_destino_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_destino_gta_seq")
    private Long id;
    
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_produtor")
    private Pessoa produtor;
    
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_estabelecimento")
    private Pessoa estabelecimento;
    
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_exploracao_propriedade")
    private Exploracao exploracao;
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
	@Column(name = "cnpj_cpf")
    private String cnpjCpf;
    
	@Column(name = "codigo_estabelecimento")
    private String codigoEstabelecimento;
    
	@Column(name = "nome_produtor")
    private String nomeProdutor;
    
	@Column(name = "nome_estabelecimento")
    private String nomeEstabelecimento;
    
	@Column(name = "codigo_exploracao")
    private String codigoExploracao;
    
	@Column(name = "codigo_aglomeracao")
    private String codigoAglomeracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getProdutor() {
        return produtor;
    }

    public void setProdutor(Pessoa produtor) {
        this.produtor = produtor;
    }

    public Pessoa getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Pessoa estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public Exploracao getExploracao() {
        return exploracao;
    }

    public void setExploracao(Exploracao exploracao) {
        this.exploracao = exploracao;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(String codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    public String getNomeProdutor() {
        return nomeProdutor;
    }

    public void setNomeProdutor(String nomeProdutor) {
        this.nomeProdutor = nomeProdutor;
    }

    public String getNomeEstabelecimento() {
        return nomeEstabelecimento;
    }

    public void setNomeEstabelecimento(String nomeEstabelecimento) {
        this.nomeEstabelecimento = nomeEstabelecimento;
    }

    public String getCodigoExploracao() {
        return codigoExploracao;
    }

    public void setCodigoExploracao(String codigoExploracao) {
        this.codigoExploracao = codigoExploracao;
    }

    public String getCodigoAglomeracao() {
        return codigoAglomeracao;
    }

    public void setCodigoAglomeracao(String codigoAglomeracao) {
        this.codigoAglomeracao = codigoAglomeracao;
    }

}