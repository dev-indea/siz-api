package br.gov.mt.indea.sizapi.webservice.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
public class Exploracao extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = -4881418006076712129L;

	@Id
    @SequenceGenerator(name = "exploracao_seq", sequenceName = "exploracao_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "exploracao_seq")
	private Long id;
    
    @Column(name = "estabelecimento_gta")
    private Integer estabelecimentoGta;
    
    @Transient
    private Integer propriedade_id;
    
    @ManyToOne
	@JoinColumn(name = "id_propriedade")
    private Propriedade propriedade;
    
    @Column(name = "orientacao_longitude")
    private String orientacaoLongitude;
    
    @Column(name = "orientacao_latitude")
    private String orientacaoLatitude;
   
    @Column(name = "latitude_grau")
    private Integer grauLatitude;
    
    @Column(name = "latitude_minuto")
    private Integer minLatitude;
    
    @Column(name = "latitude_segundo")
    private Float segLatitude;
    
    @Column(name = "longitude_grau")
    private Integer grauLongitude;
    
    @Column(name = "longitude_minuto")
    private Integer minLongitude;
    
    @Column(name = "longitude_segundo")
    private Float segLongitude;
   
    @Column(name = "codigo")
    private String codigo;
   
    @Column(name = "id_antigo")
    private String idAntigo;
    
    @Column(name = "capacidade_exploracao")
    private Integer capacidadeExploracao;
    
    @Column(name = "espolio")
    private String espolio;
    
    @Column(name = "confinamento")
    private String confinamento;
    
    @Column(name = "animais_importados")
    private String animaisImportados;
    
    @Column(name = "area_compartilhada")
    private String areaCompartilhada;
    
    @Column(name = "area_utilizada")
    private Integer areaUtilizada;
    @Column(name = "area_exploracao")
   
    private Float areaExploracao;
    
    @Column(name = "qt_pessoa_residente")
    private String qntPessoaResidente;
    
    @Column(name = "qt_pessoa_trabalha")
    private Integer qntPessoaTrabalha;
    
    @Column(name = "data_validade")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataValidade;
    
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "produtor_exploracao", joinColumns =
            @JoinColumn(name = "id_exploracao_propriedade"), inverseJoinColumns =
            @JoinColumn(name = "id_produtor"))
    private List<Produtor> produtores;
   
    public Float getSegLatitude() {
        return segLatitude;
    }

    public void setSegLatitude(Float segLatitude) {
        this.segLatitude = segLatitude;
    }

    public Float getSegLongitude() {
        return segLongitude;
    }

    public void setSegLongitude(Float segLongitude) {
        this.segLongitude = segLongitude;
    }

    public Integer getCapacidadeExploracao() {
        return capacidadeExploracao;
    }

    public void setCapacidadeExploracao(Integer capacidadeExploracao) {
        this.capacidadeExploracao = capacidadeExploracao;
    }

    public String getAreaCompartilhada() {
        return areaCompartilhada;
    }

    public void setAreaCompartilhada(String areaCompartilhada) {
        this.areaCompartilhada = areaCompartilhada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrientacaoLongitude() {
        return orientacaoLongitude;
    }

    public void setOrientacaoLongitude(String orientacaoLongitude) {
        this.orientacaoLongitude = orientacaoLongitude;
    }

    public String getOrientacaoLatitude() {
        return orientacaoLatitude;
    }

    public void setOrientacaoLatitude(String orientacaoLatitude) {
        this.orientacaoLatitude = orientacaoLatitude;
    }

    public Integer getGrauLatitude() {
        return grauLatitude;
    }

    public void setGrauLatitude(Integer grauLatitude) {
        this.grauLatitude = grauLatitude;
    }

    public Integer getMinLatitude() {
        return minLatitude;
    }

    public void setMinLatitude(Integer minLatitude) {
        this.minLatitude = minLatitude;
    }

    public Integer getGrauLongitude() {
        return grauLongitude;
    }

    public void setGrauLongitude(Integer grauLongitude) {
        this.grauLongitude = grauLongitude;
    }

    public Integer getMinLongitude() {
        return minLongitude;
    }

    public void setMinLongitude(Integer minLongitude) {
        this.minLongitude = minLongitude;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getIdAntigo() {
        return idAntigo;
    }

    public void setIdAntigo(String idAntigo) {
        this.idAntigo = idAntigo;
    }

    public Integer getAreaUtilizada() {
        return areaUtilizada;
    }

    public void setAreaUtilizada(Integer areaUtilizada) {
        this.areaUtilizada = areaUtilizada;
    }

    public Float getAreaExploracao() {
        return areaExploracao;
    }

    public void setAreaExploracao(Float areaExploracao) {
        this.areaExploracao = areaExploracao;
    }

    
    public String getQntPessoaResidente() {
        return qntPessoaResidente;
    }

    public void setQntPessoaResidente(String qntPessoaResidente) {
        this.qntPessoaResidente = qntPessoaResidente;
    }

    public Integer getQntPessoaTrabalha() {
        return qntPessoaTrabalha;
    }

    public void setQntPessoaTrabalha(Integer qntPessoaTrabalha) {
        this.qntPessoaTrabalha = qntPessoaTrabalha;
    }

    public String getEspolio() {
        return espolio;
    }

    public void setEspolio(String espolio) {
        this.espolio = espolio;
    }

    public String getConfinamento() {
        return confinamento;
    }

    public void setConfinamento(String confinamento) {
        this.confinamento = confinamento;
    }

    public String getAnimaisImportados() {
        return animaisImportados;
    }

    public void setAnimaisImportados(String animaisImportados) {
        this.animaisImportados = animaisImportados;
    }

    public Date getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }

    public Integer getEstabelecimentoGta() {
        return estabelecimentoGta;
    }

    public void setEstabelecimentoGta(Integer estabelecimentoGta) {
        this.estabelecimentoGta = estabelecimentoGta;
    }

    public Integer getPropriedade_id() {
        return propriedade_id;
    }

    public void setPropriedade_id(Integer propriedade_id) {
        this.propriedade_id = propriedade_id;
    }

    public List<Produtor> getProdutores() {
        return produtores;
    }

    public void setProdutores(List<Produtor> produtores) {
        this.produtores = produtores;
    }

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exploracao other = (Exploracao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	protected Object clone(Propriedade propriedade) throws CloneNotSupportedException{
		Exploracao cloned = (Exploracao) super.clone();
		
		cloned.setId(null);
		cloned.setPropriedade(propriedade);
		
		if (this.produtores != null){
			cloned.setProdutores(new ArrayList<Produtor>());
			for (Produtor proprietario : this.produtores) {
				cloned.getProdutores().add((Produtor) proprietario.clone());
			}
		}
		
		return cloned;
	}
	
}