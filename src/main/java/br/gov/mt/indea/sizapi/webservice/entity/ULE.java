package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;
import br.gov.mt.indea.sizapi.enums.Dominio.TipoUnidade;

@Entity(name="unidade")
public class ULE extends BaseEntity<Long> {

    private static final long serialVersionUID = -2928539765437467481L;

	@Id
    @SequenceGenerator(name = "unidade_seq", sequenceName = "unidade_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unidade_seq")
    private Long id;
    
    @Column(name = "nome", length=255)
    private String nome;
    
    @ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_unidade_pai")
    private ULE urs;
    
    @Enumerated(EnumType.STRING)
	@Column(name="tipo_unidade")
    private TipoUnidade tipoUnidade;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    public String getRegional(){
    	String a = null;
    	
    	if (this.urs == null){
    		a = this.nome.replace("REGIONAL ", "").replace("ULE ", "");
    	}else{
    		a = this.urs.getNome().replace("REGIONAL ", "").replace("ULE ", "");
    	}
    	
    	return a;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ULE getUrs() {
        return urs;
    }

    public void setUrs(ULE urs) {
        this.urs = urs;
    }
    
    public TipoUnidade getTipoUnidade() {
		return tipoUnidade;
	}

	public void setTipoUnidade(TipoUnidade tipoUnidade) {
		this.tipoUnidade = tipoUnidade;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ULE other = (ULE) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ULE [id=" + id + ", nome=" + nome + ", urs=" + urs + ", tipoUnidade=" + tipoUnidade + "]";
	}

}