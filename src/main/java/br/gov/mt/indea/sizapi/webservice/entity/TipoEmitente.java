package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name = "tipo_emitente")
public class TipoEmitente extends BaseEntity<String> implements Cloneable {

	private static final long serialVersionUID = -105549159679091557L;
	
	@Id
    @SequenceGenerator(name = "tipo_emitente_seq", sequenceName = "tipo_emitente_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_emitente_seq")
    private Long idTable;
	
	@Column(name = "identificador_tipo_emitente")
	private String id;
    
	@Column(name = "nome")
    private String nome;
    
	@Column(name = "codigo_pga")
    private String codigoPga;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
	
	protected Object clone() throws CloneNotSupportedException {
		TipoEmitente cloned = (TipoEmitente) super.clone();
		
		cloned.setIdTable(null);
		
		return cloned;
	}
	
}