package br.gov.mt.indea.sizapi.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class BaseEntity<PK extends Serializable> implements Serializable {
	
	public abstract PK getId();

    public abstract void setId(PK id);
	
	public String toString() {
	    return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
	}
	
	
}
