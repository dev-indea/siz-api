package br.gov.mt.indea.sizapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.gov.mt.indea.sizapi.entity.VisitaPropriedadeRural;

@Repository
public interface VisitaPropriedadeRuralRepository extends JpaRepository<VisitaPropriedadeRural, Long>{

	@Query("select entity "
	    +  "  from VisitaPropriedadeRural entity "
	    +  "  left join fetch entity.propriedade propriedade "
	    +  "  left join fetch propriedade.endereco "
	    +  "  left join fetch propriedade.municipio "
	    +  "  left join fetch entity.proprietario proprietario"
	    +  "  left join fetch proprietario.endereco"
	    +  "  left join fetch propriedade.coordenadaGeografica "
	    +  "  left join fetch entity.recinto r "
	    +  "  left join fetch r.endereco "
	    +  "  left join fetch r.responsavel "
	    +  "  left join fetch r.responsavelTecnico "
	    +  "  left join fetch entity.abatedouro abatedouro "
	    +  "  left join fetch abatedouro.municipio "
	    +  "  left join fetch abatedouro.coordenadaGeografica "
	    +  "  join fetch entity.listChavePrincipalVisitaPropriedadeRural cp "
	    +  " where entity.id = :id "
	    +  " order by entity.dataDaVisita ")
	
	public Optional<VisitaPropriedadeRural> findById(@Param("id") Long id);
}
