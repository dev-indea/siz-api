package br.gov.mt.indea.sizapi.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="chave_secundaria_visita")
public class ChaveSecundariaVisitaPropriedadeRural extends BaseEntity<Long>{

	private static final long serialVersionUID = -9209989053563785551L;
	
	@Id
	@SequenceGenerator(name="chave_secundaria_visita_seq", sequenceName="chave_secundaria_visita_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="chave_secundaria_visita_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_tipo_chave_secundaria_visit")
	private TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural;
	
	@ManyToOne
	@JoinColumn(name="id_chave_principal_visita")
	private ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public TipoChaveSecundariaVisitaPropriedadeRural getTipoChaveSecundariaVisitaPropriedadeRural() {
		return tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public void setTipoChaveSecundariaVisitaPropriedadeRural(
			TipoChaveSecundariaVisitaPropriedadeRural tipoChaveSecundariaVisitaPropriedadeRural) {
		this.tipoChaveSecundariaVisitaPropriedadeRural = tipoChaveSecundariaVisitaPropriedadeRural;
	}

	public ChavePrincipalVisitaPropriedadeRural getChavePrincipalVisitaPropriedadeRural() {
		return chavePrincipalVisitaPropriedadeRural;
	}

	public void setChavePrincipalVisitaPropriedadeRural(
			ChavePrincipalVisitaPropriedadeRural chavePrincipalVisitaPropriedadeRural) {
		this.chavePrincipalVisitaPropriedadeRural = chavePrincipalVisitaPropriedadeRural;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((tipoChaveSecundariaVisitaPropriedadeRural == null) ? 0
						: tipoChaveSecundariaVisitaPropriedadeRural.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChaveSecundariaVisitaPropriedadeRural other = (ChaveSecundariaVisitaPropriedadeRural) obj;
		if (tipoChaveSecundariaVisitaPropriedadeRural == null) {
			if (other.tipoChaveSecundariaVisitaPropriedadeRural != null)
				return false;
		} else if (!tipoChaveSecundariaVisitaPropriedadeRural
				.equals(other.tipoChaveSecundariaVisitaPropriedadeRural))
			return false;
		return true;
	}

}