package br.gov.mt.indea.sizapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="numero_formulario_colheita_tronco")
public class NumeroFormularioDeColheitaTroncoEncefalico extends BaseEntity<Long>{
	
	private static final long serialVersionUID = -3513472125122553076L;

	@Id
	@SequenceGenerator(name="numero_formulario_colheita_tronco_seq", sequenceName="numero_formulario_colheita_tronco_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="numero_formulario_colheita_tronco_seq")
	private Long id;
	
	@Column(name="ultimo_numero")
	private Long ultimoNumero;

	@OneToOne
	@JoinColumn(name="id_uf")
	private UF uf;
	
	@OneToOne
	@JoinColumn(name="id_servico_de_inspecao")
	private ServicoDeInspecao servicoDeInspecao;
	
	@Column(name="ano")
	private Long ano;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUltimoNumero() {
		return ultimoNumero;
	}

	public void setUltimoNumero(Long ultimoNumero) {
		this.ultimoNumero = ultimoNumero;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public ServicoDeInspecao getServicoDeInspecao() {
		return servicoDeInspecao;
	}

	public void setServicoDeInspecao(ServicoDeInspecao servicoDeInspecao) {
		this.servicoDeInspecao = servicoDeInspecao;
	}

	public Long getAno() {
		return ano;
	}

	public void setAno(Long ano) {
		this.ano = ano;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((servicoDeInspecao == null) ? 0 : servicoDeInspecao.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		result = prime * result + ((ultimoNumero == null) ? 0 : ultimoNumero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumeroFormularioDeColheitaTroncoEncefalico other = (NumeroFormularioDeColheitaTroncoEncefalico) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (servicoDeInspecao == null) {
			if (other.servicoDeInspecao != null)
				return false;
		} else if (!servicoDeInspecao.equals(other.servicoDeInspecao))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		if (ultimoNumero == null) {
			if (other.ultimoNumero != null)
				return false;
		} else if (!ultimoNumero.equals(other.ultimoNumero))
			return false;
		return true;
	}

	public void incrementNumeroFormularioDeColheitaTroncoEncefalico() {
		if (ultimoNumero == null)
			ultimoNumero = 0L;
		this.ultimoNumero++;
	}

}