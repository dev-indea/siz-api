package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import br.gov.mt.indea.sizapi.entity.Pessoa;

@Entity
@DiscriminatorValue("SERVIDOR")
public class Servidor extends Pessoa {

	private static final long serialVersionUID = 1844406149271900623L;

	/*
     * Esse campo foi criado para armazenar o c�digo da servidor que est� no campo id no webservice. 
     * Isto foi feito porque cada formIN deve referenciar uma c�pia da servidor como ela era no momento da cria��o do formIN. 
     * E isto n�o seria poss�vel de manter na forma como a servidor vem do webservice (com o id sendo chave natural)     
     */
    @Column(name="codigo_servidor_svo")
    private Long codigoServidorSVO;
	
    @ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_unidade")
    private ULE ule;
	
	private String matricula;
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH}, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_profissao")
    private Profissao profissao;

	public ULE getUle() {
		return ule;
	}

	public void setUle(ULE ule) {
		this.ule = ule;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Profissao getProfissao() {
		return profissao;
	}

	public void setProfissao(Profissao profissao) {
		this.profissao = profissao;
	}

	public Long getCodigoServidorSVO() {
		return codigoServidorSVO;
	}

	public void setCodigoServidorSVO(Long codigoServidorSVO) {
		this.codigoServidorSVO = codigoServidorSVO;
	}

}
