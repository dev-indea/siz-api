package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
public class Profissao extends BaseEntity<Long>  {

	private static final long serialVersionUID = 588022792095053355L;

	@Id
    @SequenceGenerator(name = "profissao_seq", sequenceName = "profissao_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profissao_seq")
    private Long id;
    
    private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

    }