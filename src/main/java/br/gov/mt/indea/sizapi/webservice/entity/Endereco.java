package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;

@Entity
public class Endereco extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = -1224644968820796082L;

	@Id
    @SequenceGenerator(name = "endereco_seq", sequenceName = "endereco_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "endereco_seq")
	private Long id;
    
    @Column(name = "logradouro")
    private String logradouro;
    
    @Column(name = "numero")
    private String numero;
    
    @Column(name = "bairro")
    private String bairro;
    
    @Column(name = "complemento_logradouro")
    private String complemento;
    
    @Column(name = "ponto_referecencia")
    private String referencia;
    
    @Column(name = "cep")
    private String cep;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    @Column(name = "telefone")
    private String telefone;
    
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_tipo_logradouro")
    private TipoLogradouro tipoLogradouro;
    
    public boolean isNull() {
    	return ObjectUtils.isEmpty(this);
    }
    
    public String getEnderecoToString(){
    	return this.getEnderecoToString(true, true, true, true);
    }
    
    public String getEnderecoToString(boolean logradouro, boolean numero, boolean bairro, boolean municipio){
    	StringBuilder sb = new StringBuilder();
    	
    	if (municipio && this.municipio != null)
    		sb.append(this.municipio.getNome());
    	if (logradouro && !StringUtils.isEmpty(this.logradouro))
    		sb.append(", ").append(this.logradouro);
    	if (numero && !StringUtils.isEmpty(this.numero))
    		sb.append(", n� ").append(this.numero);
    	if (bairro && !StringUtils.isEmpty(this.bairro))
    		sb.append(", ").append(this.bairro);
    	
    	
    	return sb.toString();
    }
    
    public String getFullEnderecoToString(){
    	return this.getFullEnderecoToString(true, true, true, true, true, true);
    }
    	
    public String getFullEnderecoToString(boolean logradouro, boolean numero, boolean bairro, boolean municipio, boolean complemento, boolean referencia){
    	StringBuilder sb = new StringBuilder();
    	
    	if (municipio && this.municipio != null)
    		sb.append(this.municipio.getNome());
    	if (logradouro && !StringUtils.isEmpty(this.logradouro))
    		sb.append(", ").append(this.logradouro);
    	if (numero && !StringUtils.isEmpty(this.numero))
    		sb.append(", n� ").append(this.numero);
    	if (bairro && !StringUtils.isEmpty(this.bairro))
    		sb.append(", ").append(this.bairro);
    	if (complemento && !StringUtils.isEmpty(this.complemento))
    		sb.append(", ").append(this.complemento);
    	if (referencia && !StringUtils.isEmpty(this.referencia))
    		sb.append(", ").append(this.referencia);
    	
    	return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public TipoLogradouro getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    protected Object clone() throws CloneNotSupportedException {
		Endereco cloned = (Endereco) super.clone();
		
		cloned.setId(null);
		
		if (this.tipoLogradouro != null)
			cloned.setTipoLogradouro((TipoLogradouro) this.getTipoLogradouro().clone());
		
		return cloned;
	}
}