package br.gov.mt.indea.sizapi.webservice.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.context.annotation.Lazy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;
import br.gov.mt.indea.sizapi.entity.RepresentanteEstabelecimento;
import br.gov.mt.indea.sizapi.enums.Dominio.TipoCoordenadaGeografica;

@Entity
public class Propriedade extends BaseEntity<Long> implements Cloneable {
   
    private static final long serialVersionUID = 931995233049492820L;

    @Id
    @SequenceGenerator(name = "propriedade_seq", sequenceName = "propriedade_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propriedade_seq")
    private Long id;
    
    @Column(name = "tipo_pessoa")
    private String tipoPessoa;
    
    @Column(name = "cnpj_cpf")
    private String cpfCnpj;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "apelido")
    private String apelido;
    
    @Column(name = "email_contato")
    private String email;
    
    @JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;
    
    /*
     * Esse campo foi criado para armazenar o c�digo da propriedade que est� no campo id no webservice. 
     * Isto foi feito porque cada formIN deve referenciar uma c�pia da propriedade como ela era no momento da cria��o do formIN. 
     * E isto n�o seria poss�vel de manter na forma como a propriedade vem do webservice (com o id sendo chave natural)     
     */
    @Column(name="codigo_propriedade", unique=true)
    private Long codigoPropriedade;
    
    private String codigo;
    
    @JsonIgnore
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_unidade")
    private ULE ule;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_coordenada_geografica")
    private CoordenadaGeografica coordenadaGeografica;
    
    @Enumerated(EnumType.STRING)
    @Column(name="tipo_coordenada")
    private TipoCoordenadaGeografica tipoCoordenadaGeografica;
    
    @Column(name = "via_acesso")
    private String viaAcesso;
  
    @Column(name = "endereco")
    private String enderecoPropriedade;

    @Column(name = "complemento")
    private String complemnto;
    
    @JsonIgnore
	@OneToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_representante")
	private RepresentanteEstabelecimento representanteEstabelecimento;
    
    @JsonIgnore
	@OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinTable(name = "proprietarios_propriedade", joinColumns =
            @JoinColumn(name = "id_propriedade"), inverseJoinColumns =
            @JoinColumn(name = "id_proprietario"))
    private List<Produtor> proprietarios;
    
    @JsonIgnore
	@OneToMany(mappedBy="propriedade", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
    private List<Exploracao> exploracaos;
    
    @JsonIgnore
    public String getProprietariosAsString(){
    	StringBuilder sb = null;
    	
    	if (proprietarios != null && !proprietarios.isEmpty()){
			for (Produtor produtor : proprietarios) {
				if (sb == null){
					sb = new StringBuilder();
					sb.append(produtor.getNome());
				}else
					sb.append(", ").append(produtor.getNome());
			}
    	}
		
    	if (sb != null)
    		return sb.toString();
    	else
    		return null;
    }
    
    @JsonIgnore
    public String getProdutoresAsString(){
    	StringBuilder sb = null;
    	
    	if (exploracaos != null && !exploracaos.isEmpty()){
			for (Exploracao exploracao : exploracaos) {
				
				if (exploracao.getProdutores() != null && !exploracao.getProdutores().isEmpty()){
					
					for (Produtor produtor : exploracao.getProdutores()){
						if (sb == null){
							sb = new StringBuilder();
							sb.append(produtor.getNome());
						}else
							sb.append(", ").append(produtor.getNome());
					}
				}
			}
    	}
		
    	if (sb != null)
    		return sb.toString();
    	else
    		return null;
    }
    
    @JsonIgnore
    public Exploracao getExploracao(String codigo){
    	if (this.exploracaos != null)
    		for (Exploracao exploracao : exploracaos) 
				if (exploracao.getCodigo().equals(codigo))
					return exploracao;
    	
    	return null;
    }

    public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

    public String getEnderecoPropriedade() {
        return enderecoPropriedade;
    }

    public CoordenadaGeografica getCoordenadaGeografica() {
		return coordenadaGeografica;
	}

	public void setCoordenadaGeografica(CoordenadaGeografica coordenadaGeografica) {
		this.coordenadaGeografica = coordenadaGeografica;
	}

	public List<Produtor> getProprietarios() {
        return proprietarios;
    }

    public void setProprietarios(List<Produtor> proprietarios) {
        this.proprietarios = proprietarios;
    }

    public void setEnderecoPropriedade(String enderecoPropriedade) {
        this.enderecoPropriedade = enderecoPropriedade;
    }
    
    public ULE getUle() {
        return ule;
    }

    public void setUle(ULE ule) {
        this.ule = ule;
    }    

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getViaAcesso() {
        return viaAcesso;
    }

    public void setViaAcesso(String viaAcesso) {
        this.viaAcesso = viaAcesso;
    }

    public String getComplemnto() {
        return complemnto;
    }

    public void setComplemnto(String complemnto) {
        this.complemnto = complemnto;
    }

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<Exploracao> getExploracaos() {
		return exploracaos;
	}

	public void setExploracaos(List<Exploracao> exploracaos) {
		this.exploracaos = exploracaos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public TipoCoordenadaGeografica getTipoCoordenadaGeografica() {
		return tipoCoordenadaGeografica;
	}

	public void setTipoCoordenadaGeografica(
			TipoCoordenadaGeografica tipoCoordenadaGeografica) {
		this.tipoCoordenadaGeografica = tipoCoordenadaGeografica;
	}
	
	public RepresentanteEstabelecimento getRepresentanteEstabelecimento() {
		return representanteEstabelecimento;
	}

	public void setRepresentanteEstabelecimento(RepresentanteEstabelecimento representanteEstabelecimento) {
		this.representanteEstabelecimento = representanteEstabelecimento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoPropriedade == null) ? 0 : codigoPropriedade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Propriedade other = (Propriedade) obj;
		if (codigoPropriedade == null) {
			if (other.codigoPropriedade != null)
				return false;
			else {
				if (nome == null) {
					if (other.nome != null)
						return false;
				} else if (!nome.equals(other.nome))
					return false;
				return true;
			}
		} else if (!codigoPropriedade.equals(other.codigoPropriedade))
			return false;
		return true;
	}

	public Object clone() throws CloneNotSupportedException{
		Propriedade cloned = (Propriedade) super.clone();
		
		cloned.setId(null);
		
		if (this.coordenadaGeografica != null)
			cloned.setCoordenadaGeografica((CoordenadaGeografica) this.getCoordenadaGeografica().clone());
		if (this.endereco != null)
			cloned.setEndereco((Endereco) this.getEndereco().clone());
		
		if (this.exploracaos != null){
			cloned.setExploracaos(new ArrayList<Exploracao>());
			for (Exploracao exploracao : this.exploracaos) {
				cloned.getExploracaos().add((Exploracao) exploracao.clone(cloned));
			}
		}
		
		if (this.proprietarios != null){
			cloned.setProprietarios(new ArrayList<Produtor>());
			for (Produtor proprietario : this.proprietarios) {
				cloned.getProprietarios().add((Produtor) proprietario.clone());
			}
		}
		
		return cloned;
	}

}