package br.gov.mt.indea.sizapi.enums;

public class Dominio {
	
	public enum TipoUnidade {

		ULE(1, "ULE", "Unidade Local de Execu��o"), 
		URS(2, "URS", "Unidade Regional de Supervis�o"), 
		VGA(3, "VGA", "Posto de V�rzea Grande"), 
		CBA(4, "CBA", "Unidade Metropolitana de Cuiab�");

		private long value;
		private String descricao;
		private String longDescricao;

		TipoUnidade(long value, String descricao, String longDescricao) {
			this.value = value;
			this.descricao = descricao;
			this.longDescricao = longDescricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getLongDescricao() {
			return longDescricao;
		}

		public void setLongDescricao(String longDescricao) {
			this.longDescricao = longDescricao;
		}
	}

	public enum DireitaEsquerda {
		
		DIREITA(1, "Direita"),
		ESQUERDA(2, "Esquerda");
		
		private long value;
		private String descricao;
		
		DireitaEsquerda(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum DireitoEsquerdo {
		
		DIREITO(1, "Direito"),
		ESQUERDO(2, "Esquerdo");
		
		private long value;
		private String descricao;
		
		DireitoEsquerdo(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}

	public enum NorteSul {
		NORTE(1, "Norte"),
		SUL(2, "Sul");
		
		private long value;
		private String descricao;;
		
		NorteSul(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum PositivoNegativo {
		
		ANTICOMPLEMENTAR(1, "Anticomplementar"),
		INCONCLUSIVO(2, "Inconclusivo"),
		NEGATIVO(3, "Nego"),
		POSITIVO(4, "Positivo");
		
		private long value;
		private String descricao;;
		
		PositivoNegativo(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static PositivoNegativo[] getPositivoNegativoSimples(){
			return new PositivoNegativo[]{NEGATIVO, POSITIVO};
		}
	}
	
	public enum AtivoInativo{
		ATIVO(1, "Ativo"),
		INATIVO(2, "Inativo"),
		EXPIRADO(2, "Expirado");
		
		private long value;
		private String descricao;
		
		AtivoInativo (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum AtivoPassivo{
		ATIVO(1, "Ativo"),
		PASSIVO(2, "Passivo");
		
		private long value;
		private String descricao;
		
		AtivoPassivo (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum AtivaPassiva{
		ATIVA(1, "Ativa"),
		PASSIVA(2, "Passiva");
		
		private long value;
		private String descricao;
		
		AtivaPassiva (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SimNao{
		NAO(1, "N�o", "N"),
		SIM(2, "Sim", "S");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		SimNao (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum SimNaoSI{
		NAO(1, "N�o"),
		SIM(2, "Sim"),
		SI(3, "Sem Informa��o");
		
		private long value;
		private String descricao;
		
		SimNaoSI (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SimNaoNA{
		NAO(1, "N�o"),
		SIM(2, "Sim"),
		NA(3, "N�o se aplica");
		
		private long value;
		private String descricao;
		
		SimNaoNA (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum UsoDeMedicacaoFormCOM{
		NAO(1, "N�o utilizou medica��o"),
		SIM(2, "Utilizou medica��o"),
		SI(3, "Sem Informa��o");
		
		private long value;
		private String descricao;
		
		UsoDeMedicacaoFormCOM (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum FonteDaNotificacao {
		
		PROPRIEDADE(1, "Propriedade"),
		TERCEIRO(2, "Terceiros"),
		VIGILANCIA_SVO(3, "Vigil�ncia pelo SVO"),
		MEDICO_VETERINARIO_PRIVADO(4, "M�dico veterin�rio privado"),
		FRIGORIFICO(5, "Frigor�fico"),
		CENTRO_DE_CONTROLE_ZOONOZES(6, "Centro de controle de zoonozes");
		
		private long value;
		private String descricao;
		
		FonteDaNotificacao(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum MotivoInicialDaInvestigacao {
		
		
		LESOES(1, "Les�es/achados em matadouro"),
		MORTALIDADE(2, "Mortalidade"),
		RESULTADO_DE_TESTE(3, "Resultado de teste de diagn�stico"),
		SINAIS_CLINICOS(4, "Sinais Cl�nicos"),
		VINCULO_EPIDEMIOLOGICO(5, "V�nculo Epidemiol�gico");
		
		private long value;
		private String descricao;
		
		MotivoInicialDaInvestigacao(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoPropriedade{
		ALDEIA_INDIGENA(1, "Aldeia Ind�gena"),
		ALOJAMENTO(2, "Alojamento"),
		ASSENTAMENTO(3, "Assentamento"),
		COMUNITARIO(4, "Comunit�rio"),
		CONFINAMENTO(5, "Confinamento"),
        HARAS(6, "Haras"),
		HOSPITAL_VETERINARIO(7, "Hospital/cl�nica veterin�ria"),
        JOQUEI_CLUB(8, "J�quei club"),
		LOCAL_PARA_AGLOMERACAO(9, "Local para aglomera��o"),
		PROPRIEDADE_DE_ESPERA_DE_ABATE_DE_EQUIDEOS(10, "Propriedade de espera de abate de equ�deos"),
        PROPRIEDADE_FORNECEDORA_DE_EQUIDEOS(11, "Propriedade fornecedore de equ�deos"),
        PROPRIEDADE_RURAL(12, "Propriedade rural"),
		SITIO_DE_AVES_MIGRATORIAS(13, "S�tio de aves migrat�rias"),
		SOCIEDADE_HIPICA(14,"Sociedade h�pica"),
		SOLTOS_OU_DE_PERIFERIA(15, "Soltos ou de periferia"),
		UNIDADE_DE_PESQUISA(16, "Unidade de pesquisa"),
		UNIDADE_MILITAR(17, "Unidade Militar");
		
		private long value;
		private String descricao;
		
		TipoPropriedade (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoPropriedade[] getTipoPropriedadeFormIN(){
			return new TipoPropriedade[]{ALDEIA_INDIGENA, 
					                     ASSENTAMENTO, 
					                     COMUNITARIO, 
					                     CONFINAMENTO, 
					                     HOSPITAL_VETERINARIO, 
					                     LOCAL_PARA_AGLOMERACAO, 
					                     PROPRIEDADE_RURAL, 
					                     SITIO_DE_AVES_MIGRATORIAS, 
					                     SOLTOS_OU_DE_PERIFERIA, 
					                     UNIDADE_DE_PESQUISA, 
					                     UNIDADE_MILITAR};
		}
		
		public static TipoPropriedade[] getTipoPropriedadeEquinos(){
			return new TipoPropriedade[]{ALDEIA_INDIGENA,
										 ALOJAMENTO,
					                     ASSENTAMENTO, 
					                     COMUNITARIO,  
					                     HARAS,
					                     HOSPITAL_VETERINARIO,
					                     JOQUEI_CLUB,
					                     LOCAL_PARA_AGLOMERACAO, 
					                     PROPRIEDADE_DE_ESPERA_DE_ABATE_DE_EQUIDEOS,
					                     PROPRIEDADE_FORNECEDORA_DE_EQUIDEOS,
					                     PROPRIEDADE_RURAL,
					                     SITIO_DE_AVES_MIGRATORIAS,
					                     SOCIEDADE_HIPICA, 
					                     UNIDADE_DE_PESQUISA, 
					                     UNIDADE_MILITAR};
		}
	}
	
	public enum SistemaDeCriacaoPropriedade{
		EXTENSIVO(1, "Extensivo"),
		INTENSIVO(2, "Intensivo"),
		SEMI_INTENSIVO(3, "Semi-intensivo"),
		NAO_SE_APLICA(4, "N�o se aplica");
		
		private long value;
		private String descricao;
		
		SistemaDeCriacaoPropriedade (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SistemaDeCriacaoDeRuminantes{
		EXTENSIVO(1, "Extensivo"),
		SEMI_INTENSIVO(2, "Intensivo/Semi-intensivo"),
		CONFINAMENTO(3, "Confinamento");
		
		private long value;
		private String descricao;
		
		SistemaDeCriacaoDeRuminantes (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoCoordenadaGeografica {
		
		SAD_69(1, "SAD 69"),
		SIRGAS_2000(2, "SIRGAS 2000"),
		WGS_84(3, "WGS 84");
		
		private long value;
		private String descricao;
		
		TipoCoordenadaGeografica(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoCoordenadaGeografica[] getListaTipoCoordenadaGeograficaFormSN(){
			return new TipoCoordenadaGeografica[]{SIRGAS_2000, WGS_84};
		}
	}
	
	
	public enum FuncaoNoEstabelecimento {
		
		FUNCIONARIO(1, "Funcion�rio (administrador, capataz, caseiro, etc)"),
		MEDICO_VETERINARIO(2, "M�dico Veterin�rio"),
		PARENTE(3, "Parente"),
		PRODUTOR(4, "Produtor"),
		PROPRIETARIO(5, "Propriet�rio");
		
		private long value;
		private String descricao;
		
		FuncaoNoEstabelecimento (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	
	public enum VigilanciaSindromica {
		
		HEMORRAGICA_DOS_SUINOS(1, "Hemorr�gica dos Su�nos"),
		NERVOSA(2, "Nervosa"),
		NERVOSA_OU_RESPIRATORIA_DAS_AVES(3, "Nervosa ou Respirat�ria das Aves"),
		VESICULAR(4, "Vesicular"),
		PATOLOGIA_GRANULOMATOSA_INTERNA(5, "Patologia granulomatosa interna"),
		PATOLOGIA_REPRODUTIVA_DOS_BOVINOS(6, "Patologia reprodutiva dos bovinos"),
		ANEMIA_INFECCIOSA_EQUINA(7, "Anemia infecciosa equina"),
		DOENCA_RESPIRATORIA_DOS_EQUINOS(8, "Doen�a respirat�ria dos equinos"),
		NENHUMA_DAS_ANTERIORES(9, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		VigilanciaSindromica (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static VigilanciaSindromica[] getListaDoencasNaoPrevistaNoFormIN(){
			return new VigilanciaSindromica[]{PATOLOGIA_GRANULOMATOSA_INTERNA,
					              			  PATOLOGIA_REPRODUTIVA_DOS_BOVINOS,
					              			  ANEMIA_INFECCIOSA_EQUINA,
					              			  DOENCA_RESPIRATORIA_DOS_EQUINOS,
					              			  NENHUMA_DAS_ANTERIORES};
		}
	}

	
	public enum TipoTelefone{
		
		CELULAR(1, "Celular"),
		COMERCIAL(2, "Comercial"),
		RESIDENCIAL(3, "Residencial");
		
		private long value;
		private String descricao;
		
		TipoTelefone (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	
	public enum FaixaEtariaOuEspecie{
		ATE_6(1, "At� 6 m"),
		MAIOR_6(2, "> 6 m"),
		ATE_12(3, "At� 12 m"),
		MAIOR_12(4, "> 12 m"),
		ENTRE_13_24(5, "13 a 24 m"),
		ENTRE_25_36(6, "25 a 36 m"),
		MAIOR_36(7, "> 36 m"),
		ENTRE_37_48(8, "37 a 48 m"),
		ENTRE_49_60(9, "49 a 60 m"),
		ENTRE_61_72(10, "61 a 72 m"),
		ENTRE_73_84(11, "73 a 84 m"),
		ENTRE_85_96(12, "84 a 96 m"),
		ENTRE_97_108(13, "97 a 108 m"),
		ENTRE_109_120(14, "109 a 120 m"),
		ENTRE_121_132(15, "121 a 132 m"),
		ENTRE_133_144(16, "133 a 144 m"),
		ENTRE_145_156(17, "145 a 156 m"),
		ENTRE_157_168(18, "157 a 168 m"),
		ENTRE_169_180(19, "169 a 180 m"),
		ENTRE_181_192(20, "181 a 192 m"),
		ENTRE_193_204(21, "193 a 204 m"),
		ENTRE_205_216(22, "205 a 216 m"),
		ENTRE_217_228(23, "217 a 228 m"),
		ENTRE_229_240(24, "229 a 240 m"),
		
		GALINHAS(25, "Galinhas"),
		PERU(26, "Peru"),
		ANSERIFORMES(27, "Anseriformes"),
		RATITAS(28, "Ratitas"),
		CODORNAS(29, "Codornas"),
		PERDIZ(30, "Perdizes"),
		GALINHA_D_ANGOLA(31, "Galinhas D'Angola"),
		PSITACIFORMES(32, "Psitaciformes"),
		AVES_SILVESTRES(33, "Aves Silvestres"),
		PASSERIFORMES(34, "Passeriformes"),
		FAISAO(35, "Fais�o"),
		
		COLMEIAS(36, "Colm�ias"),
		
		COELHOS(37, "Coelhos"),

		NENHUM(38, "--"),
		OUTRAS_AVES(39, "Outras Aves"),
		
		CACHACO_MATRIZ(40, "Cacha�o/Matriz"),
		LEITAO(41, "Leit�o(oa)"),
		DEMAIS_SUINOS(42, "Demais");
		
		private long value;
		private String descricao;
		
		FaixaEtariaOuEspecie (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static FaixaEtariaOuEspecie[] getBovinos(){
			return new FaixaEtariaOuEspecie[]{ATE_12, ENTRE_13_24, ENTRE_25_36, MAIOR_36};
		}
		
		public static FaixaEtariaOuEspecie[] getBubalinos(){
			return new FaixaEtariaOuEspecie[]{ATE_12, ENTRE_13_24, ENTRE_25_36, MAIOR_36};
		}
		
		public static FaixaEtariaOuEspecie[] getCaprinos(){
			return new FaixaEtariaOuEspecie[]{ATE_12, MAIOR_12};
		}
		
		public static FaixaEtariaOuEspecie[] getOvinos(){
			return new FaixaEtariaOuEspecie[]{ATE_12, MAIOR_12};
		}
		
		public static FaixaEtariaOuEspecie[] getSuinos(){
			return new FaixaEtariaOuEspecie[]{CACHACO_MATRIZ, LEITAO, DEMAIS_SUINOS, ATE_6, MAIOR_6};
		}
		
		public static FaixaEtariaOuEspecie[] getEquinos(){
			return new FaixaEtariaOuEspecie[]{ATE_6, MAIOR_6};
		}
		
		public static FaixaEtariaOuEspecie[] getAsininos(){
			return new FaixaEtariaOuEspecie[]{ATE_6, MAIOR_6};
		}
		
		public static FaixaEtariaOuEspecie[] getMuares(){
			return new FaixaEtariaOuEspecie[]{ATE_6, MAIOR_6};
		}
		
		public static FaixaEtariaOuEspecie[] getAves(){
			return new FaixaEtariaOuEspecie[]{GALINHAS, 
											  PERU, 
											  ANSERIFORMES, 
											  RATITAS,
											  OUTRAS_AVES};
		}
		
		public static FaixaEtariaOuEspecie[] getOutrasAves(){
			return new FaixaEtariaOuEspecie[]{AVES_SILVESTRES,
											  CODORNAS,
											  GALINHA_D_ANGOLA,
											  FAISAO,
											  PASSERIFORMES,
											  PERDIZ,
											  PSITACIFORMES};
		}
		
		public static FaixaEtariaOuEspecie[] getAbelhas(){
			return new FaixaEtariaOuEspecie[]{COLMEIAS};
		}
		
		public static FaixaEtariaOuEspecie[] getLagomorfos(){
			return new FaixaEtariaOuEspecie[]{COELHOS};
		}
		
		public static FaixaEtariaOuEspecie[] getOutrosAnimais(){
			return new FaixaEtariaOuEspecie[]{NENHUM};
		}
		
		public static FaixaEtariaOuEspecie[] getFaixaEtariaFormSN(){
			return new FaixaEtariaOuEspecie[]{ATE_12,
											  ENTRE_13_24,
											  ENTRE_25_36,
											  ENTRE_37_48,
											  ENTRE_49_60,
											  ENTRE_61_72,
											  ENTRE_73_84,
											  ENTRE_85_96,
											  ENTRE_97_108,
											  ENTRE_109_120,
											  ENTRE_121_132,
											  ENTRE_133_144,
											  ENTRE_145_156,
											  ENTRE_157_168,
											  ENTRE_169_180,
											  ENTRE_181_192,
											  ENTRE_193_204,
											  ENTRE_205_216,
											  ENTRE_217_228,
											  ENTRE_229_240};
		}
	}
	
	
	public enum TipoDestinoExploracao{
		COMERCIO_ANIMAIS(1, "Com�rcio de animais", "1"),
		COMERCIO_PRODUTOS(2, "Com�rcio de produtos", "2"),
		CONSUMO_PROPRIO(3, "Consumo Pr�prio", "3"),
		PRODUCAO_BIOLOGICOS(4, "Produ��o de biol�gicos", "4"),
		COMPANHIA(5, "Companhia", "5"),
		ESPORTE_LAZER(6, "Esporte/Lazer", "6"),
		TRABALHO(7, "Trabalho", "7");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoDestinoExploracao (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	
	public enum MedidasAdotadasNoEstabelecimento{
		
		COMBATE(1, "Combate a vetores"),
		DESINTERDICAO(2, "Desinterdi��o"),
		DESTRUICAO_DE_PRODUTOS(3, "Destrui��o de Protudos"),
		INTERDICAO(4, "Interdi��o"),
		INTRODUCAO_DE_SENTINELAS(5, "Introdu��o de sentinelas"),
		ISOLAMENTO(6, "Isolamento de animais"),
		LIMPEZA(7, "Limpeza e desinfec��o"),
		SEQUESTRO_DE_PRODUTOS(8, "Sequestro de Produtos"),
		VACINACAO(9, "Vacina��o"),
		VAZIO_SANITARIO(10, "Vazio sanit�rio");
		
		private long value;
		private String descricao;
		
		MedidasAdotadasNoEstabelecimento (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static MedidasAdotadasNoEstabelecimento[] getFormINValues(){
			return new MedidasAdotadasNoEstabelecimento[]{DESTRUICAO_DE_PRODUTOS, 
														  COMBATE, 
														  INTERDICAO,
														  INTRODUCAO_DE_SENTINELAS,
														  ISOLAMENTO, 
														  LIMPEZA, 
														  SEQUESTRO_DE_PRODUTOS, 
														  VACINACAO, 
														  VAZIO_SANITARIO};
		}
		
		public static MedidasAdotadasNoEstabelecimento[] getFormCOMValues(){
			return new MedidasAdotadasNoEstabelecimento[]{COMBATE,
														  DESINTERDICAO,
														  DESTRUICAO_DE_PRODUTOS,
														  INTERDICAO,
														  INTRODUCAO_DE_SENTINELAS,
														  ISOLAMENTO, 
														  LIMPEZA, 
														  SEQUESTRO_DE_PRODUTOS,
														  VACINACAO, 
														  VAZIO_SANITARIO};
		}
	}
	
	
	public enum MomentoMedidasAdotadasNoEstabelecimento{
		INICIO(1, "In�cio"),
		EM_CURSO(2, "Em curso"),
		TERMINO(3, "T�rmino");
		
		private long value;
		private String descricao;
		
		MomentoMedidasAdotadasNoEstabelecimento (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	
	public enum TipoTransitoDeAnimais{
		INGRESSO(1, "Ingresso"),
		EGRESSO(2, "Egresso");
		
		private long value;
		private String descricao;
		
		TipoTransitoDeAnimais (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum FinalidadeExploracaoBovinosEBubalinos{
		CORTE(1, "Corte"),
		LEITE(2, "Leite"),
		MISTA(3, "Mista");
		
		private long value;
		private String descricao;
		
		FinalidadeExploracaoBovinosEBubalinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static FinalidadeExploracaoBovinosEBubalinos[] getFinalidadeExploracaoBovinosEBubalinos(){
			return new FinalidadeExploracaoBovinosEBubalinos[]{CORTE,
														       LEITE,
													 	       MISTA}; 
		}
	}
	
	public enum TipoExploracaoOutrosRuminantes{
		CAPRINO_OVINOCULTURA(1, "Caprino/Ovinocultura"),
		OUTRO(2, "Outro");
		
		private long value;
		private String descricao;
		
		TipoExploracaoOutrosRuminantes (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum FinalidadeExploracaoCaprinos{
		CORTE(1, "Corte"),
		LEITE(2, "Leite"),
		MISTA(3, "Mista"),
		NDA(4, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		FinalidadeExploracaoCaprinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	
	public enum FinalidadeExploracaoOvinos{
		CORTE(1, "Corte"),
		LA(2, "L�"),
		LEITE(3, "Leite"),
		MISTA(4, "Mista");
		
		private long value;
		private String descricao;
		
		FinalidadeExploracaoOvinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	
	public enum FaseExploracaoBovinosEBubalinos{
		CICLO_COMPLETO(1, "Ciclo completo"),
		CRIA_RECRIA(2, "Cria/Recria"),
		ENGORDA(3, "Engorda"),
		SUBSISTENCIA(4, "Subsist�ncia"),
		TERMINACAO(5, "Termina��o"),
		NDA(6, "Nenhuma das anteriores"),
		CRIA(7, "Cria"),
		RECRIA(8, "Recria");
		
		private long value;
		private String descricao;
		
		FaseExploracaoBovinosEBubalinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_FormIN(){
			return new FaseExploracaoBovinosEBubalinos[]{CICLO_COMPLETO,
														 CRIA_RECRIA,
														 ENGORDA,
														 SUBSISTENCIA,
														 TERMINACAO,
														 NDA};
		}
		
		public static FaseExploracaoBovinosEBubalinos[] getListaFaseExploracaoBovinosEBubalinos_Vigilancia(){
			return new FaseExploracaoBovinosEBubalinos[]{CRIA,
														 RECRIA,
														 ENGORDA};
		}
	}
	
	
	public enum FaseExploracaoCaprinos{
		CICLO_COMPLETO(1, "Ciclo completo"),
		CRIA(2, "Cria/Recria"),
		ENGORDA(3, "Engorda"),
		SUBSISTENCIA(4, "Subsist�ncia"),
		TERMINACAO(5, "Termina��o"),
		NDA(6, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		FaseExploracaoCaprinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum FaseExploracaoOvinos{
		CICLO_COMPLETO(1, "Ciclo completo"),
		CRIA(2, "Cria/Recria"),
		ENGORDA(3, "Engorda"),
		PRODUCAO_DE_LA(4, "Produ��o de l�"),
		SUBSISTENCIA(5, "Subsist�ncia"),
		TERMINACAO(6, "Termina��o"),
		NDA(7, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		FaseExploracaoOvinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoExploracaoSuinos{
		CICLO_COMPLETO(1, "Ciclo completo"),
		CRECHE(2, "Creche"),
		CRIATORIO_SUBSISTENCIA(3, "Criat�rio/Subsist�ncia"),
		GRSC(4, "GRSC"),
		NDA(5, "Nenhuma das anteriores"),
		RECRIA(6, "Recria"),
		TERMINACAO(7, "Terminacao"),
		UPL(8, "UPL");
		
		private long value;
		private String descricao;
		
		TipoExploracaoSuinos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoExploracaoSuinos[] getTiposSubsistencia(){
			return new TipoExploracaoSuinos[]{CRIATORIO_SUBSISTENCIA};
		}
		
		public static TipoExploracaoSuinos[] getTiposGranja(){
			return new TipoExploracaoSuinos[]{CICLO_COMPLETO, UPL, CRECHE, RECRIA, TERMINACAO, GRSC};
		}
	}
	
	public enum FinalidadeExploracaoSuinos{
		SUBSISTENCIA("Subsist�ncia"),
		CICLO_COMPLETO("Ciclo completo"),
		QUARENTENARIO("Quarenten�rio"),
		UNIDADE_PRODUTORA_DE_LEITAO("Unidade produtora de leit�o"),
		UNIDADE_DE_TERMINACAO("Unidade de termina��o"),
		OUTROS("Outros");
		
		private String descricao;
		
		FinalidadeExploracaoSuinos (String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoExploracaoSuinosSistemaIndustrial{
		CRIA(1, "Cria"),
		ENGORDA(2, "Engorda"),
		RECRIA(3, "Recria"),
		NAO_EXISTE(3, "N�o existe");
		
		private long value;
		private String descricao;
		
		TipoExploracaoSuinosSistemaIndustrial (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum TipoExploracaoAvicolaSistemaIndustrial{
		CORTE(1, "Corte"),
		POSTURA(2, "Postura"),
		NAO_EXISTE(3, "N�o existe"),
		OUTROS(4, "Outro");
		
		private long value;
		private String descricao;
		
		TipoExploracaoAvicolaSistemaIndustrial (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
		
	
	public enum TipoExploracaoEquideos{
		HARAS(1, "Haras"),
		JOQUEI_CLUBE(2, "J�quei Clube"),
		PROPRIEDADE_DE_ESPERA_DE_ABATE(3, "Propriedade de espera de abate"),
		PROPRIEDADE_FORNECEDORA_DE_EQUIDEOS(4, "Propriedade fornecedora de equ�deos"),
		SOCIEDADE_HIPICA(5, "Sociedade H�pica"),
		UNIDADE_MILITAR(6, "Unidade Militar"),
		NDA(7, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		TipoExploracaoEquideos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoExploracaoAves{
		AVOSEIRO(1, "Avoseiro"),
		BISAVOSEIRO(2, "Bisavoseiro"),
		CICLO_COMPLETO(3, "Ciclo Completo"),
		CICLO_PARCIAL(4, "Ciclo Parcial"),
		COMERCIAL_CORTE(5, "Comercial corte"),
		COMERCIAL_POSTURA(6, "Comercial postura"),
		CRIA_RECRIA(7, "Cria/Recria"),
		ENGORDA(8, "Engorda"),
		INCUBATORIO(9, "Incubat�rio"),
		LINHA_PURA(10, "Linha pura"),
		MATRIZEIRO(11, "Matrizeiro"),
		PRODUCAO_DE_OVOS_CONTROLADOS(12, "Produ��o de ovos controlados"),
		RECRIA_DE_POSTURA(13, "Recria de postura"),
		RECRIA_DE_REPRODUCAO(14, "Recria de reprodu��o"),
		REPRODUCAO(15, "Reprodu��o"),
		SPF(16, "SPF"),
		SUBSISTENCIA(17, "Subsist�ncia"),
		NDA(18, "Nenhuma das anteriores");
		
		private long value;
		private String descricao;
		
		TipoExploracaoAves (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoExploracaoAbelhas{
		APITOXINA(1, "Apitoxina"),
		CERA(2, "Cera"),
		EXTRATO_DE_PROPOLIS(3, "Extrato de pr�polis"),
		GELEIA_REAL(4, "Geleia real"),
		MEL(5, "Mel"),
		POLEN(6, "P�len"),
		POLINIZACAO(7, "Poliniza��o"),
		PROPOLIS(8, "Pr�polis"),
		RAINHA(9, "Rainha");
		
		private long value;
		private String descricao;
		
		TipoExploracaoAbelhas (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoExploracaoCoelhos{
		ANIMAL_DE_LABORATORIO(1, "Animal de Laborat�rio"),
		COMERCIO_DE_PELE_OU_PELO(2, "Com�rcio de pele ou pelo"),
		GENETICA(3, "Gen�tica"),
		PRODUCAO_DE_CARNE(4, "Produ��o de carne");
		
		private long value;
		private String descricao;
		
		TipoExploracaoCoelhos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoInvestigacaoFormCOM{
		ENCERRAMENTO(1, "Encerramento"),
		INTERMEDIARIA(2, "Intermedi�ria");
		
		private long value;
		private String descricao;
		
		TipoInvestigacaoFormCOM (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum ProvavelOrigemFormCOM{
		NAO_IDENTIFICADA(1, "N�o identificada"),
		ORIGEM_ANTERIOR_CONFIRMADA(2, "Origem anterior confirmada"),
		OUTRA(3, "Outra");
		
		private long value;
		private String descricao;
		
		ProvavelOrigemFormCOM (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoOcorrenciaObservadaFormCOM{
		DESCAMINHO(5, "Descaminho/Contrabando", "5"),
		FUGA_OU_EXTRAVIO(4, "Fuga ou extravio", "4"),
		FURTO(3, "Furto", "3"),
		MORTE_POR_OUTRA_CAUSA(2, "Morte por outra causa", "2"),
		NASCIMENTO(1, "Nascimento", "1");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoOcorrenciaObservadaFormCOM (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum TipoVinculoEpidemiologico{
		ANIMAIS_SILVESTRES_OU_CRIADOS_SOLTOS(1, "Animais silvestres ou outros animais criados soltos", "Animais silvestres ou outros animais criados soltos", 14),
		CESSAO_DE_INSTALACOES(2, "Cess�o de instala��oes para manejo de animais do estabelecimento investigado", "Cess�o de instala��oes ", 7),
		EMPRESTIMO_DE_ANIMAIS(3, "Empr�stimo de animais (reprodutores ou de trabalho)", "Empr�stimo de animais ", 22),
		ENVIO_DE_ANIMAIS_PARA_ESTABELECIMENTO_INVESTIGADO(4, "Envio de animais para o estabelecimento investigado", "Envio de animais para o estabelecimento investigado", 12),
		FATORES_ECOLOGICOS(5, "Fatores ecol�gicos, incluindo presen�a de vetores ou hospedeiros silvestres, bem como a presen�a de abrigos de morcegos hemat�fagos", "Fatores ecol�gicos", 10),
		INGRESSO_DE_PESSOAS(6, "Ingresso de pessoas (m�dicos veterin�rios, t�cnicos agr�colas, trabalhadores rurais, parentes, etc) que tiveram contato com animais do estabelecimento", "Ingresso de pessoas que tiveram contato com animais do estabelecimento sob investiga��o", 9),
		INGRESSO_DE_VEICULOS(7, "Ingresso de ve�culos que passaram pelo estabelecimento sob investiga��o", "Ingresso de ve�culos que passaram pelo estabelecimento sob investiga��o", 21),
		INTERCAMBIO_DE_PRODUTOS(8, "Interc�mbio de produtos ou subprodutos de origem animal com estabelecimento investigado", "Interc�mbio de produtos ou subprodutos de origem animal com o estabelecimento investigado", 6),
		INVESTIGACAO_OU_RECOMENDACAO_DE_DENUNCIA(9, "Investiga��o originada por den�ncia ou recomenda��o de pessoas do estabelecimento sob investiga��o ou demais estabelecimentos com v�nculo epidemiol�gico", "Investiga��o originada por den�ncia ou recomenda��o", 8),
		MEDICAMENTOS_EM_COMUM(10, "Medicamentos ou vacinas em comum", "Medicamentos ou vacinas em comum", 13),
		MESMA_FONTE_DE_AGUA(11, "Mesma fonte d'�gua", "Mesma fonte d'�gua", 4),
		MESMA_ORIGEM_DOS_ANIMAIS(12, "Mesma origem dos animais", "Mesma origem dos animais", 15),
		MESMA_ORIGEM_FONTE_DE_ALIMENTOS(13, "Mesma origem/fonte de alimentos", "Mesma origem/fonte de alimentos", 3),
		MESMO_PROPRIETARIO(14, "Mesmo propriet�rio ou produtor", "Mesmo propriet�rio ou produtor", 5),
		PARTICIPACAO_EM_MESMO_EVENTO(15, "Participa��o em mesmo evento com aglomera��o", "Participa��o em mesmo evento com aglomera��o", 18),
		PROXIMIDADE_GEOGRAFICA(16, "Proximidade geogr�fica", "Proximidade geogr�fica", 2),
		RECEPCAO_DE_ANIMAIS(17, "Recep��o de animais do estabelecimento investigado", "Recep��o de animais do estabelecimento investigado", 11),
		RELACAO_GENEALOGICA(18, "Rela��o geneal�gica", "Rela��o geneal�gica", 20),
		USO_DE_EQUIPAMENTOS(19, "Uso de equipamentos ou ferramentas do estabelecimento investigado", "Uso de equipamentos ou ferramentas do estabelecimento investigado", 17),
		USO_DE_INSTALACOES(20, "Uso de instala��es do estabelecimento investigado para manejar seus animais", "Uso de instala��es do estabelecimento investigado para manejar seus animais", 16),
		VISITA_AO_ESTABELECIMENTO_INVESTIGADO(21, "Algu�m deste estabelecimento visitou o estabelecimento sob investiga��o", "Algu�m deste estabelecimento visitou o estabelecimento sob investiga��o", 19),
		VIZINHO_DE_CERCA(22, "Vizinho de cerca", "Vizinho de cerca", 1);
		
		private long value;
		private String descricao;
		private String descricaoAbreviada;
		private long valorFormVIN;
		
		TipoVinculoEpidemiologico (long value, String descricao, String descricaoAbreviada, long valorFormVIN){
			this.value = value;
			this.descricao = descricao;
			this.descricaoAbreviada = descricaoAbreviada;
			this.valorFormVIN = valorFormVIN;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getDescricaoAbreviada() {
			return descricaoAbreviada;
		}

		public void setDescricaoAbreviada(String descricaoAbreviada) {
			this.descricaoAbreviada = descricaoAbreviada;
		}

		public long getValorFormVIN() {
			return valorFormVIN;
		}

		public void setValorFormVIN(long valorFormVIN) {
			this.valorFormVIN = valorFormVIN;
		}
	}
	
	public enum TipoDeAgrupamento{
		APIARIO(1, "Api�rio", "AP"),
		APRISCO(2, "Aprisco", "AR"),
		BAIAS(3, "Baias", "BA"),
		ESTABULO(4, "Est�bulo", "ES"),
		GALPOES(5, "Galp�es", "GA"),
		NUCLEOS(6, "N�cleos", "NU"),
		PASTOS(7, "Pastos", "PA"),
		PIQUETES(8, "Piquetes", "PI");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoDeAgrupamento (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum Especie{
		ANIMAL_AQUATICO(0, "Animal Aqu�tico", "AQU"),
		ANIMAL_SILVESTRE(1, "Animal Silvestre", "FAU"),
		ABELHA(2, "Abelha", "API"),
		ASININA(3, "Asinina", "ASI"),
		AVESTRUZ(4, "Avestruz", "AVZ"),
		AVE_ORNAMENTAL(32, "Ave ornamental", "ORN"),
		AVICOLA(35, "Av�cola", "AVI"),
		AQUICOLA(36, "Aqu�cola", "AQI"),
		BOVINA(5, "Bovina", "BOV"),
		BUBALINA(6, "Bubalina", "BUB"),
		CAMELO(7, "Camelo", "CAM"),
		CANIDEO(8, "Can�deo", "CAN"),
		CAPRINA(9, "Caprina", "CAP"),
		CODORNA(10, "Codorna", "COD"),
		EMA(11, "Ema", "EMA"),
		EQUINA(12, "Equina", "EQU"),
		FAISAO(13, "Fais�o", "FAI"),
		FELIDEO(14, "Felideo", "FEL"),
		GALINACEO(15, "Galin�ceo", "GAL"),
		GALINHA_ANGOLA(16, "Galinha d'Angola", "ANG"),
		GANSO(17, "Ganso", "GAN"),
		LAGOMORFO(18, "Lagomorfo", "LAG"),
		MARRECO(19, "Marreco", "MAR"),
		MORCEGO_HEMATOFAGO(20, "Morcego hemat�fago", "M-HEM"), //
		MORCEGO_NAO_HEMATOFAGO(31, "Morcego n�o-hemat�fago", "MNao-HEM"), //
		MUAR(22, "Muar", "MUA"),
		OVINA(23, "Ovina", "OVI"),
		OUTROS_PALMIPEDE(24, "Outro Palm�pede", "PAL"),
		OUTRAS_AVES(34, "Outras aves", "OUT"),
		PASSERIFORME(25, "Passeriforme", "PAS"),
		PATO(26, "Pato", "PAT"),
		PERU(27, "Peru", "PER"),
		PERDIZ(28, "Perdiz", "PEZ"),
		PSCITACIFORMES(29, "Psitaciformes", "PSI"),
		RATITAS(33, "Ratitas", "RAT"),
		SUINA(30, "Su�na", "SUI"),
		ZEBRA(31, "Zebra", "ZEB");
		
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		Especie (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
		
		public static Especie[] getEspeciesLAB(){
			return new Especie[]{ANIMAL_SILVESTRE,
								 ABELHA,
								 ASININA,
								 AVESTRUZ,
								 AVE_ORNAMENTAL,
								 BOVINA,
								 BUBALINA,
								 CAMELO,
								 CANIDEO,
								 CAPRINA,
								 CODORNA,
								 EMA,
								 EQUINA,
								 FAISAO,
								 FELIDEO,
								 GALINACEO,
								 GALINHA_ANGOLA,
								 GANSO,
								 LAGOMORFO,
								 MARRECO,
								 MORCEGO_HEMATOFAGO,
								 MORCEGO_NAO_HEMATOFAGO,
								 MUAR,
								 OVINA,
								 OUTROS_PALMIPEDE,
								 OUTRAS_AVES,
								 PASSERIFORME,
								 PATO,
								 PERU,
								 PERDIZ,
								 PSCITACIFORMES,
								 RATITAS,
								 SUINA,
								 ZEBRA};
		}
		
		public static Especie[] getEspeciesSV(){
			return new Especie[]{ANIMAL_SILVESTRE,
								 BOVINA, 
								 BUBALINA, 
								 CAMELO,
								 CAPRINA,
								 EQUINA,
								 MUAR,
								 OVINA, 
								 SUINA};
		}
		
		public static Especie[] getEspeciesSN(){
			return new Especie[]{ANIMAL_SILVESTRE, 
								 BOVINA, 
								 BUBALINA, 
								 CANIDEO,CAPRINA,EQUINA, 
								 FELIDEO,OVINA, 
								 MORCEGO_HEMATOFAGO, 
								 MORCEGO_NAO_HEMATOFAGO, 
								 SUINA};
		}
		
		public static Especie[] getEspeciesDeEquinos(){
			return new Especie[]{ASININA, 
								 EQUINA, 
								 MUAR, 
								 ZEBRA};
		}
		
		public static Especie[] getEspeciesDeAves(){
			return new Especie[]{GALINACEO,
					             GALINHA_ANGOLA,
					             PATO,
					             PERU,
					             GANSO,
					             CODORNA,
					             MARRECO,
					             AVE_ORNAMENTAL,
					             RATITAS,
					             FAISAO,
					             OUTRAS_AVES};
		}
		
		public static Especie[] getEspeciesInvestigacaoEpidemiologica(){
			return new Especie[]{AVICOLA,
							     AQUICOLA,
								 BOVINA, 
								 BUBALINA, 
								 CAPRINA,
								 EQUINA,
								 OVINA, 
								 SUINA};
		}
		
		public static Especie[] getEspeciesAfetadasFormIN(){
			return new Especie[]{ANIMAL_AQUATICO,
								 BOVINA, 
								 BUBALINA, 
								 CAPRINA,
								 OVINA, 
								 SUINA,
								 EQUINA,
								 ASININA,
								 MUAR,
								 AVICOLA,
								 ABELHA,
								 LAGOMORFO};
		}
		
		public static Especie[] getEspeciesFormularioDeColheitaEET(){
			return new Especie[]{BOVINA, 
								 BUBALINA, 
								 CAPRINA,
								 OVINA};
		}
	}
	
	public enum Sexo{
		MASCULINO(1, "Masculino", "M"),
		FEMININO(2, "Feminino", "F"),
		P1_3(3, "1� ter�o da gesta��o", "P1/3"),
		P2_3(4, "2� ter�o da gesta��o", "P2/3"),
		P3_3(5, "3� ter�o da gesta��o", "P3/3"),
		FP1_3(6, "1� ter�o da gesta��o", "FP1/3"),
		FP2_3(7, "2� ter�o da gesta��o", "FP2/3"),
		FP3_3(8, "3� ter�o da gesta��o", "FP3/3");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		Sexo (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
		
		public static Sexo[] getSexoSimples(){
			return new Sexo[]{MASCULINO, FEMININO};
		}
		
		public static Sexo[] getSexo_FormLAB(){
			return new Sexo[]{MASCULINO, FEMININO, P1_3, P2_3, P3_3};
		}

		public static Sexo[] getSexo_FormEQ(){
			return new Sexo[]{MASCULINO, FEMININO, FP1_3, FP2_3, FP3_3};
		}
		
		public static Sexo[] getSexo_PeriodoGestacao(){
			return new Sexo[]{P1_3, P2_3, P3_3};
		}
	}
	
	public enum MachoFemea {
		MACHO(1, "Macho"),
		FEMEA(2, "F�mea");
		
		private long value;
		private String descricao;
		
		MachoFemea(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoPeriodo{
		DIAS(1, "Dias", "D"),
		MESES(2, "Mes", "M"),
		ANOS(3, "Anos", "A");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoPeriodo (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
		
		public static TipoPeriodo[] getListaTipoPeriodos_AnoMes(){
			return new TipoPeriodo[]{ANOS, MESES};
		}
		
		public static TipoPeriodo[] getListaTipoPeriodos_DiaMes(){
			return new TipoPeriodo[]{DIAS, MESES};
		}
		
		public static TipoPeriodo[] getListaTipoPeriodos_DiaAno(){
			return new TipoPeriodo[]{DIAS, ANOS};
		}
	}
	
	public enum TipoCriacaoAnimais {
		
		PASTO_EXTENSIVO(1, "A pasto/extensivo"),
		CONFINADOS(2, "Confinados"),
		SEMI_CONFINADOS(3, "Semiconfinados");
		
		private long value;
		private String descricao;
		
		TipoCriacaoAnimais (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoReposicaoAnimais {
		
		TERCEIROS(1, "Oriunda de terceiros"),
		PROPRIA(2, "Reposi��o pr�pria");
		
		private long value;
		private String descricao;
		
		TipoReposicaoAnimais (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoOrigemRacaoAnimais {
		
		COMERCIAL(1, "Comercial"),
		FABRICACAO_PROPRIA(2, "Fabrica��o pr�pria"),
		NA(3, "N�o se aplica");
		
		private long value;
		private String descricao;
		
		TipoOrigemRacaoAnimais (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoOrigemRestosDeComida {
		
		RESIDENCIAL(1, "Residencial"),
		RESTAURANTES(2, "Restaurantes"),
		OUTRAS(3, "Outras"),
		NA(3, "N�o se aplica");
		
		private long value;
		private String descricao;
		
		TipoOrigemRestosDeComida (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoOrigemSoroOuRestosDeLavoura {
		
		FABRICACAO_PROPRIA(1, "Fabrica��o Pr�pria"),
		OUTRAS_PROPRIEDADES(2, "Outras propriedades"),
		NA(3, "N�o se aplica");
		
		private long value;
		private String descricao;
		
		TipoOrigemSoroOuRestosDeLavoura (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum CategoriaAves{
		AVOZEIRO(1, "Avozeiro"),
		BISAVOZEIRO(2, "Bizavozeiro"),
		COMERCIAL_CORTE(3, "Comercial corte"),
		COMERCIAL_POSTURA(4, "Comercial postura"),
		LINHA_PURA(5, "Linha pura"),
		MATRIZEIRO(6, "Matrizeiro"),
		PRODUCAO_DE_OVOS_CONTROLADOS(7, "Produ��o de ovos controlados"),
		RECRIA_DE_POSTURA(8, "Recria de postura"),
		RECRIA_DE_REPRODUCAO(9, "Recria de reprodu��o"),
		SITIO(10, "S�tio de aves migrat�rias"),
		SPF(11, "SPF"),
		SUBSISTENCIA(12, "Subsist�ncia");
		
		private long value;
		private String descricao;
		
		CategoriaAves (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SinaisClinicosSindromeVesicular{
		BOLHAS(1, "Bolhas", "BO"),
		CROSTAS(2, "Crostas", "CR"),
		EROSOES_SEM_CICATRIZACAO(3, "Eros�es sem cicatriza��es", "ER"),
		LESOES_COM_CICATRIZACAO_PARCIAL(4, "Les�es com cicatriza��o parcial", "LCP"),
		LESOES_CICATRIZADAS(5, "Les�es cicatrizadas", "LC"),
		PÚSTULAS(6, "P�stulas", "PU"),
		VESICULAS_INTEGRAS(7, "Ves�culas �ntegras", "VI"),
		VESICULAS_RECEM_ROMPIDAS_COM_EPITELIO(8, "Ves�culas rec�m rompidas com epit�lio", "VRcomEp"),
		VESICULAS_RECEM_ROMPIDAS_SEM_EPITELIO(9, "Ves�culas rec�m rompidas sem epit�lio", "VRsemEp");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		SinaisClinicosSindromeVesicular (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum SinaisClinicosSindromeHemorragica {
		
		ANOREXIA(1, "Anorexia"),
		APETITE_IRREGULAR(2, "Apetite irregular"),
		CIANOSE(3, "Cianose"),
		CONJUNTIVITE(4, "Conjuntivite"),
		CONSTIPACAO_INTESTINAL(5, "Constipa��o instestinal, seguida de diarr�ia"),
		CONVULSAO(6, "Convuls�o"),
		CORRIMENTO_NASAL_PURULENTO(7, "Corrimento nasal purulento"),
		DIARREIA(8, "Diarreia"),
		DISPNEIA(9, "Dispneia"),
		ENFARTAMENTO_GANGLIONAR(10, "Enfartamento ganglionar"),
		FEBRE(13, "Febre"),
		HIPEREMIA_MULTIFOCAL(12, "Hiperemia multifocal"),
		LETARGIA(13, "Letargia"),
		MOVIMENTO_DE_PEDALAGEM(14, "Movimento de pedalagem"),
		NADA_OBSERVADO(15, "Nada observado"),
		PARESIA(16, "Paresia"),
		PROSTRACAO(17, "Prostra��o"),
		PRURIDO(18, "Prurido"),
		RECUPERACAO_APARENTE(19, "Recupera��o aparente, com reca�da e morte"),
		REPETICAO_DE_CIO(20, "Repeti��o de cio"),
		TOSSE(21, "Tosse"),
		TAQUIPNEIA(22, "Taquipneia");
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesEstadoGeral(){
			return new SinaisClinicosSindromeHemorragica[]{ANOREXIA, 
					   									   APETITE_IRREGULAR,
					   									   CONJUNTIVITE,
					   									   FEBRE, 
														   PROSTRACAO, 
														   RECUPERACAO_APARENTE,
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaRespiratorio(){
			return new SinaisClinicosSindromeHemorragica[]{CORRIMENTO_NASAL_PURULENTO,
					   									   DISPNEIA, 
														   TAQUIPNEIA, 
														   TOSSE, 
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaNervoso(){
			return new SinaisClinicosSindromeHemorragica[]{CONVULSAO, 
														   LETARGIA, 
														   MOVIMENTO_DE_PEDALAGEM,
														   PARESIA, 
														   PRURIDO, 
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaDigestorio(){
			return new SinaisClinicosSindromeHemorragica[]{CONSTIPACAO_INTESTINAL,
														   DIARREIA,
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaReprodutivo(){
			return new SinaisClinicosSindromeHemorragica[]{REPETICAO_DE_CIO,
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaTegumentar(){
			return new SinaisClinicosSindromeHemorragica[]{CIANOSE, 
														   HIPEREMIA_MULTIFOCAL, 
														   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeHemorragica[] getLesoesSistemaLinfatico(){
			return new SinaisClinicosSindromeHemorragica[]{ENFARTAMENTO_GANGLIONAR, 
														   NADA_OBSERVADO};
		}
		
		private long value;
		private String descricao;
		
		SinaisClinicosSindromeHemorragica (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SinaisClinicosSindromeNervosaERespiratoriaDasAves {
		
		ANDAR_EM_CIRCULOS(1, "Andar em c�rculos"),
		ANOREXIA(2, "Anorexia"),
		ASAS_CAIDAS(3, "Asas ca�das"),
		ATAXIA(4, "Ataxia"),
		CONJUNTIVITE(5, "Conjuntivite"),
		CORIZA(6, "Coriza"),
		DEPRESSAO(7, "Depress�o"),
		DESIDRATACAO(8, "Desidrata��o"),
		DIARREIA_AMARELADA(9, "Diarr�ia amarelada"),
		DIARREIA_AQUOSA(10, "Diarr�ia aquosa"),
		DIARREIA_ESVERDEADA(11, "Diarr�ia Esverdeada"),
		DIARREIA_SANGUINOLENTA(12, "Diarr�ia sanguinolenta"),
		DIARREIA_SEROSA(13, "Diarr�ia serosa"),
		DISPNEIA(14, "Dispneia"),
		EDEMA_E_CIANOSE_NAS_CRISTAS_BARBELAS(15, "Edema e cianose nas cristas/barbelas"),
		EDEMA_E_MANCHA_VERMELHA_NAS_PATAS(16, "Edema e mancha vermelha nas patas"),
		EDEMA_FACIAL(17, "Edema facial e de cabe�a"),
		ESPIRROS(18, "Espirros"),
		ESTERTORES(19, "Estertores"),
		HEMORRAGIA_PETEQUIAS_EQUIMOSE_NA_PELE(20, "Hemorragia/pet�quias/equimose na pele"),
		OVOS_MAL_FORMADOS(21, "Ovos mal formados"),
		LACRIMEJAMENTO(22, "Lacrimejamento"),
		PARALISIA_DAS_PATAS(23, "Paralisia das patas"),
		PARALISIA_TOTAL(24, "Paralisia total"),
		PENAS_ARREPIADAS(25, "Penas arrepiadas"),
		TREMORES(26, "Tremores"),
		TOSSE(27, "Tosse"),
		TORCICOLO(28, "Torcicolo"),
		NADA_OBSERVADO(29, "Nada observado");
		
		private long value;
		private String descricao;
		
		SinaisClinicosSindromeNervosaERespiratoriaDasAves (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getSinaisClinicosSindromeNervosa_EstadoGeral(){
			return new SinaisClinicosSindromeNervosaERespiratoriaDasAves[]{ANOREXIA, 
																		   CONJUNTIVITE,  
																		   DESIDRATACAO, 
																		   DEPRESSAO, 
																		   LACRIMEJAMENTO,
																		   OVOS_MAL_FORMADOS,
																		   PENAS_ARREPIADAS, 
																		   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getSinaisClinicosSindromeNervosa_SistemaRespiratorio(){
			return new SinaisClinicosSindromeNervosaERespiratoriaDasAves[]{CORIZA, 
																		   DISPNEIA, 
																		   ESPIRROS,
																		   ESTERTORES,
																		   TOSSE, 
																		   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getSinaisClinicosSindromeNervosa_SistemaNervoso(){
			return new SinaisClinicosSindromeNervosaERespiratoriaDasAves[]{ANDAR_EM_CIRCULOS, 
																		   ASAS_CAIDAS, 
																		   ATAXIA,
																		   PARALISIA_DAS_PATAS, 
																		   PARALISIA_TOTAL,
																		   TREMORES, 
																		   TORCICOLO, 
																		   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getSinaisClinicosSindromeNervosa_SistemaDigestorio(){
			return new SinaisClinicosSindromeNervosaERespiratoriaDasAves[]{DIARREIA_AMARELADA, 
																		   DIARREIA_AQUOSA, 
																		   DIARREIA_ESVERDEADA, 
																		   DIARREIA_SANGUINOLENTA, 
																		   DIARREIA_SEROSA, 
																		   NADA_OBSERVADO};
		}
		
		public static SinaisClinicosSindromeNervosaERespiratoriaDasAves[] getSinaisClinicosSindromeNervosa_SistemaCirculatorio(){
			return new SinaisClinicosSindromeNervosaERespiratoriaDasAves[]{EDEMA_E_CIANOSE_NAS_CRISTAS_BARBELAS, 
																		   EDEMA_FACIAL, 
																		   EDEMA_E_MANCHA_VERMELHA_NAS_PATAS,
																		   HEMORRAGIA_PETEQUIAS_EQUIMOSE_NA_PELE, 
																		   NADA_OBSERVADO};
		}
	}
	
	public enum SinaisClinicosExameMaleina {
		
		BLEFAROESPASMO(1, "Blefaroespasmo"),
		DOR_NO_LOCAL_DA_INOCULACAO(2, "Dor no local da inocula��o"),
		EDEMA_PALPEBRAL(3, "Edema palpebral"),
		EDEMA_PALPEBRAL_DISCRETO(4, "Edema palpebral discreto"),
		FOTOFOBIA(5, "Fotofobia"),
		SECRECAO_NASAL(6, "Secre��o nasal"),
		SECRECAO_OCULAR_VISTOSA_OU_PURULENTA(7, "Secre��o ocular vistosa ou purulenta");
		
		private long value;
		private String descricao;
		
		SinaisClinicosExameMaleina (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum ResultadoNecropsiaDeAves {
		
		AEROSSACOLITE(1, "Aerossacolite"),
		CONGESTAO_EDEMA_HEMORRAGIA_PULMONAR(3, "Congest�o/edema/hemorragia pulmonar"),
		CONGESTAO_DA_MUSCULATURA(4, "Congest�o da musculatura"),
		DEPOSITO_DE_URATOS_NOS_TUBULOS(7, "Dep�sito de uratos nos t�bulos"),
		EDEMA_HEMORRAGIAS_PETEQUIAS_NO_ENCEFALO_OU_CEREBELO(5, "Edemas/hemorragias/pet�quias no enc�falo ou cerebelo"),
		EDEMA_SUBCUTANEO_NA_CABECA_OU_PESCOCO(8, "Edema subcut�neo na cabe�a ou pesco�o"),
		EXSUDATO_TRAQUEAL(10, "Exsudato traqueal"),
		HEMORRAGIA_DO_TRATO_INTESTINAL(1, "Hemorragia do trato intestinal"),
		HEMORRAGIA_EDEMA_DEGENERACAO_DOS_OVARIOS(11, "Hemorragia/edema/degenera��o dos ov�rios"),
		HEMORRAGIA_INFLAMACAO_NO_CORACAO(1, "Hemorragias/inflama��o no cora��o"),
		INFLAMACAO_DA_TRAQUEIA(1, "Inflama��o da traqu�ia"),
		LARINGE_OU_TRAQUEIA_HEMORRAGICAS(12, "Laringe/traqueia hemorr�gicas"),
		NECROSE_HEMORRAGIA_NA_MOELA_E_PROVENTRICULO(1, "Necrose/hemorragia na moela e proventr�culo"),
		ORGAOS_COM_CONGESTAO_SIST_CIRCULATORIO(15, "�rg�os com congest�o ou hemorragias"),
		ORGAOS_COM_CONGESTAO_SIST_DIGESTIVO(15, "�rg�os com congest�o ou hemorragias"),
		ORGAOS_COM_CONGESTAO_SIST_URINARIO(16, "�rg�os com congest�o ou hemorragias"),
		PERITONITE(17, "Peritonite"),
		PETEQUIAS_NA_PLEURA_OU_PERITONIO(18, "Pet�quias na pleura/perit�nio"),
		PETEQUIAS_NA_SUPERFICIE_ABDOMINAL(19, "Pet�quias na superf�cie abdominal"),
		SACO_AEREO_TURVO(20, "Saco a�reo turvo"),
		SECRECAO_ORAL(1, "Secre��o oral"),
		SECRECOES_NA_CAVIDADE_NASAL(21, "Secre��es na cavidade nasal"),
		SINUSITE(22, "Sinusite"),
		NADA_OBSERVADO(23, "Nada observado");
		
		private long value;
		private String descricao;
		
		ResultadoNecropsiaDeAves (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_EstadoGeral(){
			return new ResultadoNecropsiaDeAves[]{CONGESTAO_DA_MUSCULATURA, 
												  EDEMA_SUBCUTANEO_NA_CABECA_OU_PESCOCO, 
												  PETEQUIAS_NA_SUPERFICIE_ABDOMINAL, 
												  NADA_OBSERVADO};
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_SistemaRespiratorio(){
			return new ResultadoNecropsiaDeAves[]{AEROSSACOLITE, 
												  CONGESTAO_EDEMA_HEMORRAGIA_PULMONAR, 
												  EXSUDATO_TRAQUEAL, 
												  INFLAMACAO_DA_TRAQUEIA,
												  LARINGE_OU_TRAQUEIA_HEMORRAGICAS,
												  SACO_AEREO_TURVO, 
												  SECRECOES_NA_CAVIDADE_NASAL, 
												  SINUSITE, 
												  NADA_OBSERVADO};
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_SistemaUrinarioEReprodutor(){
			return new ResultadoNecropsiaDeAves[]{DEPOSITO_DE_URATOS_NOS_TUBULOS, 
												  HEMORRAGIA_EDEMA_DEGENERACAO_DOS_OVARIOS,
												  ORGAOS_COM_CONGESTAO_SIST_URINARIO, 
												  NADA_OBSERVADO};
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_SistemaCirculatorioHematopoteicoELinfatico(){
			return new ResultadoNecropsiaDeAves[]{ORGAOS_COM_CONGESTAO_SIST_CIRCULATORIO, 
												  HEMORRAGIA_INFLAMACAO_NO_CORACAO,
												  PERITONITE, 
												  PETEQUIAS_NA_PLEURA_OU_PERITONIO,
												  NADA_OBSERVADO};
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_SistemaDigestivo(){
			return new ResultadoNecropsiaDeAves[]{SECRECAO_ORAL,
												  HEMORRAGIA_DO_TRATO_INTESTINAL,
												  NECROSE_HEMORRAGIA_NA_MOELA_E_PROVENTRICULO,
												  ORGAOS_COM_CONGESTAO_SIST_DIGESTIVO,
												  NADA_OBSERVADO};
		}
		
		public static ResultadoNecropsiaDeAves[] getResultadoNecropsiaDeAves_SistemaNervoso(){
			return new ResultadoNecropsiaDeAves[]{EDEMA_HEMORRAGIAS_PETEQUIAS_NO_ENCEFALO_OU_CEREBELO,
												  NADA_OBSERVADO};
		}
		
	}
	
	public enum SinaisClinicosCavidadeNasal {
		CICATRIZ(1, "Cicatriz"),
		NODULOS(2, "N�dulos"),
		ULCERAS(3, "�lceras");
		
		private long value;
		private String descricao;
		
		SinaisClinicosCavidadeNasal (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum SinaisClinicosPele {
		CROSTAS(1, "Crostas"),
		NODULOS(2, "N�dulos"),
		ULCERAS(3, "�lceras");
		
		private long value;
		private String descricao;
		
		SinaisClinicosPele (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum LocalDeColhimentoDeAmostra {
		
		AGLOMERACOES(1, "Aglomera��es"),
		ESTABELECIMENTO_DE_CRIACAO(2, "Estabelecimento de cria��o"),
		HOSPITAL_VETERINARIO(3, "Hospital veterin�rio"),
		OUTROS(4, "Outros");
		
		private long value;
		private String descricao;
		
		LocalDeColhimentoDeAmostra (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum MetodoParaEstipularIdade {
		
		CRONOLOGIA_DENTARIA(1, "Cronologia dent�ria ou cornual"),
		INFORMADO_PELO_ESTABELECIMENTO(2, "Informado pelo respons�vel no Estabelecimento"),
		MARCACAO_DA_VACINA_CONTRA_BRUCELOSE(3, "Marca��o da vacina contra brucelose"),
		REGISTRO_GENEALOGICO(4, "Registro geneal�gico");
		
		private long value;
		private String descricao;
		
		MetodoParaEstipularIdade (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum CategoriaDoAnimalSubmetidoAVigilancia {
		COM_SINAIS_CLINICOS_DE_DOENCA_NERVOSA(1, "Com dist�rbio neurol�gico/locomotor/comportamental"),
		COM_DOENCA_CRONICA(2, "Com doen�a cr�nica, caquetizante ou depauperante"),
		EM_DECUBITO(3, "Em dec�bito ou que n�o se locomove sem ajuda"),
		ENCONTRADO_MORTO(4, "Encontrado morto na fazenda ou no transporte"),
		NAO_APLICAVEL(5, "N�o aplic�vel para amostras de campo"),
		BOVINO_OU_BUBALINO_IMPORTADO(6, "Bovino ou bubalino importado de pa�s de risco para EEB"),
		COM_VINCULO_EPIDEMIOLOGICO(7, "Com v�nculo epidemiol�gico de investiga��o de EET");
		
		private long value;
		private String descricao;
		
		CategoriaDoAnimalSubmetidoAVigilancia (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoAmostra {
		
		ENCEFALO(1, "Enc�falo"),
		MEDULA(2, "Medula"),
		VISCERAS(3, "V�sceras"),
		OUTRAS(4, "Outras");
		
		private long value;
		private String descricao;
		
		TipoAmostra (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum MeioConservacaoAmostra {
		
		CONGELADO(1, "Congelado"),
		FORMOLIZADO(2, "Formolizado"),
		GLICERINA(3, "Glicerina a 50%"),
		REFRIGERADO(4, "Refrigerado");
		
		private long value;
		private String descricao;
		
		MeioConservacaoAmostra (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoAlimentoAves {
		
		RACAO_ADQUIRIDA_NA_LOJA(1, "Ra��o adquirida na loja"),
		RACAO_FORNECIDA_PELA_INTEGRADORA_OU_COOPERATIVA(2, "Ra��o fornecida pela integradora ou coopera"),
		OUTRO(3, "Outro");
		
		private long value;
		private String descricao;
		
		TipoAlimentoAves (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoAlimentoSuideos {
		
		RACAO_COMERCIAL(1, "Ra��o comercial"),
		RACAO_PREPARADA_NA_PROPRIEDADE(2, "Ra��o preparada na propriedade"),
		RESTO_DE_ALIMENTOS(3, "Resto de alimentos"),
		OUTRO(4, "Outro");
		
		private long value;
		private String descricao;
		
		TipoAlimentoSuideos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoOrigemSuideos {
		
		GRANJA_GRSC(1, "Granja GRSC"),
		PROPRIO_PLANTEL(2, "Pr�prio plantel"),
		VIZINHO(3, "Vizinho"),
		OUTRO(4, "Outro");
		
		private long value;
		private String descricao;
		
		TipoOrigemSuideos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoDestinoSuideos {
		
		ABATE_SIF(1, "Abate  SIF"),
		ABATE_SISE(2, "Abate SISE"),
		ABATE_SIM(3, "Abate SIM"),
		CONSUMO_PROPRIO(4, "Consumo pr�prio"),
		REPRODUCAO(5, "Reprodu��o"),
		AGLOMERACOES(6, "Aglomera��es"),
		CRIA_RECRIA_ENGORDA(7, "Cria, recria, engorda"),
		ABATE_SEM_INSPECAO(8, "Abate sem inspe��o"),
		OUTRO(9, "Outro");
		
		private long value;
		private String descricao;
		
		TipoDestinoSuideos (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoDestinoCadaveres {
		
		ENTERRIO(1, "Enterrio"),
		COMPOSTEIRA(2, "Composteira"),
		BIODIGESTOR(3, "Biodigestor"),
		OUTRO(4, "Outro");
		
		private long value;
		private String descricao;
		
		TipoDestinoCadaveres (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoLaboratorio {
		
		CREDENCIADO(1, "Laborat�rio credenciado"),
		CREDENCIADO_PUBLICO(2, "Laborat�rio credenciado p�blico"),
		OFICIAL(3, "Laborat�rio oficial");
		
		private long value;
		private String descricao;
		
		TipoLaboratorio (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum FinalidadeDoTesteDeLaboratorio {
		
		CONTROLE_DO_PLANTEL(1, "Controle do plantel"),
		ESTUDO_EPIDEMIOLOGICO_NAO_OFICIAL(2, "Estudo epidemiol�gico n�o oficial"),
		ESTUDO_EPIDEMIOLOGICO_OFICIAL(3, "Estudo epidemiol�gico oficial"),
		RETESTE(4, "Reteste"),
		SANEAMENTO_OU_INVESTIGACAO_DE_FOCO_OU_SUSPEITA(5, "Saneamento/investiga��o de foco ou suspeita"),
		TRANSITO(6, "Tr�nsito");
		
		private long value;
		private String descricao;
		
		FinalidadeDoTesteDeLaboratorio (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static FinalidadeDoTesteDeLaboratorio[] getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciado(){
			return new FinalidadeDoTesteDeLaboratorio[]{CONTROLE_DO_PLANTEL, 
														ESTUDO_EPIDEMIOLOGICO_NAO_OFICIAL, 
														TRANSITO};
		}
		
		public static FinalidadeDoTesteDeLaboratorio[] getFinalidadeDoTesteDeLaboratorio_LaboratorioCredenciadoPublico(){
			return new FinalidadeDoTesteDeLaboratorio[]{ESTUDO_EPIDEMIOLOGICO_OFICIAL, 
														SANEAMENTO_OU_INVESTIGACAO_DE_FOCO_OU_SUSPEITA, 
														TRANSITO};
		}
		
		public static FinalidadeDoTesteDeLaboratorio[] getFinalidadeDoTesteDeLaboratorio_LaboratorioOficial(){
			return new FinalidadeDoTesteDeLaboratorio[]{ESTUDO_EPIDEMIOLOGICO_OFICIAL, 
														RETESTE, 
														SANEAMENTO_OU_INVESTIGACAO_DE_FOCO_OU_SUSPEITA};
		}
	}
	
	public enum TipoDeTesteParaMormo {
		
		FC(1, "Fixa��o de Complemento", "FC"),
		WB(2, "Western Blot", "WB");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoDeTesteParaMormo (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}

	public enum LocalizacaoCorrimentoNasal {
		
		UNILATERAL(1, "Unilateral", "1"),
		BILATERAL(2, "Bilateral", "2");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		LocalizacaoCorrimentoNasal (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum TipoCorrimentoNasal {
		
		SEROSO(1, "Seroso", "SE"),
		MUCOSO(2, "Mucoso", "MU"),
		PURULENTO(3, "Purulento,", "PU");
		
		private long value;
		private String descricao;
		private String abreviatura;
		
		TipoCorrimentoNasal (long value, String descricao, String abreviatura){
			this.value = value;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}
	}
	
	public enum OrigemDoAnimal {
		
		NASCIDO_NO_ESTABELECIMENTO(1, "Nascido no estabelecimento"),
		ORIUNDO_DE_OUTRO_ESTABELECIMENTO_DO_MUNICIPIO(2, "Oriundo de outro estabelecimento do munic�pio"),
		ORIUNDO_DE_OUTRO_ESTABELECIMENTO_DO_ESTADO(3, "Oriundo de outro munic�pio do estado"),
		ORIUNDO_DE_OUTRO_ESTADO(4, "Oriundo de outro estado");
		
		private long value;
		private String descricao;
		
		OrigemDoAnimal(long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum DoencasRespiratorias {
		
		BOTRIOMICOSE(1, "Botriomicose"),
		ESPOROTRICOSE(2, "Esporotricose"),
		GARROTILHO(3, "Garrotilho"),
		INFLUENZA(4, "Influenza"),
		LINFANGITE(5, "Linfangite");
		
		
		private long value;
		private String descricao;
		
		DoencasRespiratorias (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum ManejoDosAnimais {
		
		ALIMENTACAO_RICA_EM_CARBOIDRATOS(1, "Alimenta��o rica em carboidratos"),
		USO_COMPARTILHADO_DE_COCHOS_E_BEBEDOUROS(2, "Uso compartilhado de cochos e bebedouros"),
		USO_COMPARTILHADO_DE_FOMITES(3, "Uso compartilhado de f�mites");
		
		private long value;
		private String descricao;
		
		ManejoDosAnimais (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum StatusFormIN {
		
		NOVO(1, "Novo"),
		VALIDADO(2, "Validado"),
		ALTERADO(3, "Alterado"),
		COPIA_VALIDADA(4, "C�pia Validada");
		
		private long value;
		private String descricao;
		
		StatusFormIN (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum PontosDeRisco {
		
		AEROPORTO(1, "Aeroporto"),
		ASSENTAMENTO(2, "Assentamento"),
		ATERRO_SANITARIO(3, "Aterro sanit�rio"),
		DIVISA_ENTRE_ESTADOS(4, "Divisa entre estados"),
		ESTABELECIMENTO_DE_PROCESSAMENTO_PRODUTOS_ANIMAIS(5, "Estabelecimento de processamento de produtos de origem animal"),
		ESTACAO_FERROVIARIA(6, "Esta��o ferrovi�ria"),
		ESTRADA_BOIADEIRA(7, "Estrada boiadeira"),
		FRIGORIFICO(8, "Frigor�fico"),
		FRONTEIRA_ATE_15KM(9, "Fronteira at� 15 km"),
		LINHA_FRONTEIRA(10, "Linha fronteira"),
		LIXAO(11, "Lix�o"),
		PORTO(12, "Porto"),
		POUSO_BOIADA(13, "Pouso boiada"),
		RECINTO(14, "Recinto"),
		RESERVA_INDIGENA(15, "Reserva ind�gena"),
		RESERVAS_NATURAIS(16, "Reservas naturais"),
		RODOVIARIA(17, "Rodovi�ria"),
		SITIO_DE_INVERNADA_DE_AVE_MIGRATORIA(18, "S�tio de invernada de ave migrat�ria"),
		ZOOLOGICO(19, "Zool�gico");
		
		private long value;
		private String descricao;
		
		PontosDeRisco (long value, String descricao){
			this.value = value;
			this.descricao = descricao;
		}

		public long getValue() {
			return value;
		}

		public void setValue(long value) {
			this.value = value;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum TipoEstabelecimento {
		
		ABATEDOURO("Abatedouro"),
		PROPRIEDADE("Propriedade"),
		RECINTO("Recinto");
		
		private String descricao;
		
		TipoEstabelecimento(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoEstabelecimento[] getListaTipoEstabelecimento_VisitaPropriedadeRural(){
			return new TipoEstabelecimento[]{PROPRIEDADE, 
											 RECINTO};
		}
		
		public static TipoEstabelecimento[] getListaTipoEstabelecimento_FormIN(){
			return new TipoEstabelecimento[]{ABATEDOURO, 
											 PROPRIEDADE};
		}
	}
	
	public enum TipoResponsavelColheitaDeAmostra {
		
		SERVICO_VETERINARIO_OFICIAL("Servi�o veterin�rio oficial"),
		MEDICO_VETERINARIO_PRIVADO("M�dico veterin�rio privado"),
		SERVICO_OFICIAL("Servi�o oficial - sa�de, prefeitura etc"),
		OUTRO("Outro");
		
		private String descricao;
		
		TipoResponsavelColheitaDeAmostra(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoAlteracaoSindromeNeurologica {
		
		NEUROLOGICA_OU_DE_SENSIBILIDADE("Altera��o neurol�gica ou de sensibilidade"),
		POSTURA_OU_LOCOMOCAO("Altera��o de postura ou locomo��o"),
		COMPORTAMENTAL("Altera��o comportamental"),
		MORTE_SUBITA("Morte s�bita");
		
		private String descricao;
		
		TipoAlteracaoSindromeNeurologica(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoDisturbioSindromeNeurologica {
		
		CEGUEIRA("Cegueira"),
		CONVULSOES("Convuls�es"),
		DISMETRIA("Dismetria"),
		FOTOFOBIA_AEROFOBIA("Fotofobia/aerofobia"),
		ESPASMOS_MUSCULARES("Espasmos musculares"),
		HIPERESTESIA_AO_SOM_TOQUE_OU_LUZ("Hiperstesia ao som, ao toque ou � luz"),
		MIDRIASE("Midr�ase"),
		MOVIMENTOS_DE_PEDALAGEM("Movimentos de pedalagem"),
		NISTAGMO("Nistagmo"),
		OPISTOTONO("Opist�tono"),
		PRIAPISMO("Priapismo"),
		PARALISIA_FLACIDA_DOS_MEMBROS("Paralisia fl�cida dos membros anteriores e/ou posteriores"),
		PARALISIA_ALERTA("Paralisia (mas alerta)"),
		POSICIONAMENTO_ANORMAL_DA_CABECA_OU_ORELHAS("Posicionamento anormal da cabe�a ou das orelhas"),
		SIALORREIA("Sialorr�ia"),
		TETANIA("Tetania"),
		TREMOR("Tremor"),
		TENESMO("Tenesmo"),
		
		ANDAR_EM_CIRCULOS("Andar em c�rculos"),
		ATAXIA("Ataxia"),
		DOBRAMENTO_DO_BOLETO("Dobramento do boleto"),
		INCOORDENACAO("Incoordenacao"),
		PROSTRACAO("Prostracao"),
		QUEDA_FREQUENTE_SEM_MOTIVACAO_APARENTE("Queda frequente sem motiva��o aparente"),
		
		AGRESSIVIDADE("Agressividade"),
		EXCITABILIDADE_OU_MEDO("Excitabilidade ou medo (sem motica��o aparente)"),
		APETITE_ANOMALO("Apetite an�malo"),
		COICEAR_ANORMAL_E_PERSISTENTE_QUANDO_ORDENHADA("Coicear anormal e persistente quando ordenhada"),
		CONSCIENCIA_ALTERADA("Consci�ncia alterada"),
		DEPRESSAO("Depress�o"),
		HESITACAO_EM_PORTAS_PORTOES_BARREIRAS("Hesita��o em portas, port�es, barreiras"),
		LAMBEDURA_ANORMAL_E_EXCESSIVA_NO_NARIZ_E_FLANCO("Lambedura anormal e excessiva do nariz e flanco"),
		MUDANCA_DE_HIERARQUIA_NO_REBANHO("Mudan�a de hierarquia no rebanho"),
		RANGER_DE_DENTES("Ranger de dentes");
		
		public static TipoDisturbioSindromeNeurologica[] getListaTiposDeAlteracaoNeurologicaOuDeSensibilidade(){
			return new TipoDisturbioSindromeNeurologica[]{CEGUEIRA,
													 CONVULSOES,
													 DISMETRIA,
													 FOTOFOBIA_AEROFOBIA,
													 ESPASMOS_MUSCULARES,
													 HIPERESTESIA_AO_SOM_TOQUE_OU_LUZ,
													 MIDRIASE,
													 MOVIMENTOS_DE_PEDALAGEM,
													 NISTAGMO,
													 OPISTOTONO,
													 PRIAPISMO,
													 PARALISIA_FLACIDA_DOS_MEMBROS,
													 PARALISIA_ALERTA,
													 POSICIONAMENTO_ANORMAL_DA_CABECA_OU_ORELHAS,
													 SIALORREIA,
													 TETANIA,
													 TREMOR,
													 TENESMO};
		}
		
		public static TipoDisturbioSindromeNeurologica[] getListaTiposDeAlteracaoDePosturaOuLocomocao(){
			return new TipoDisturbioSindromeNeurologica[]{ANDAR_EM_CIRCULOS,
													 ATAXIA,
													 DOBRAMENTO_DO_BOLETO,
													 INCOORDENACAO,
													 PROSTRACAO,
													 QUEDA_FREQUENTE_SEM_MOTIVACAO_APARENTE};
		}
		
		public static TipoDisturbioSindromeNeurologica[] getListaTiposDeAlteracaoComportamental(){
			return new TipoDisturbioSindromeNeurologica[]{AGRESSIVIDADE,
													 EXCITABILIDADE_OU_MEDO,
													 APETITE_ANOMALO,
													 COICEAR_ANORMAL_E_PERSISTENTE_QUANDO_ORDENHADA,
													 CONSCIENCIA_ALTERADA,
													 DEPRESSAO,
													 HESITACAO_EM_PORTAS_PORTOES_BARREIRAS,
													 LAMBEDURA_ANORMAL_E_EXCESSIVA_NO_NARIZ_E_FLANCO,
													 MUDANCA_DE_HIERARQUIA_NO_REBANHO,
													 RANGER_DE_DENTES};
		}
		
		private String descricao;
		
		TipoDisturbioSindromeNeurologica(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	}
	
	public enum TipoDiagnostico {
		
		PROVAVEL("Prov�vel"),
		CONCLUSIVO("Conclusivo"),
		LABORATORIAL("LABORATORIAL");
		
		private String descricao;
		
		TipoDiagnostico(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public static TipoDiagnostico[] getListTipoDiagnostico_ProvavelConclusivo(){
			return new TipoDiagnostico[]{PROVAVEL, CONCLUSIVO};
		}
		
		public static TipoDiagnostico[] getListTipoDiagnostico_ConclusivoLaboratorial(){
			return new TipoDiagnostico[]{CONCLUSIVO, LABORATORIAL};
		}
		
	}
	
	public enum TipoTesteLaboratorial {
		
		ELISA("Elisa"),
		FIXACAO_DE_COMPLEMENTO("Fixa��o de complemento"),
		IDGA("IDGA"),
		IFD("IFD"),
		IMUNO_HISTOQUIMICA("Imuno-histoqu�mica"),
		ISOLAMENTO("Isolamento"),
		PCR("PCR"),
		SOROLOGICO("Sorol�gico"),
		PROVA_BIOLOGICA("Prova biol�gica"),
		TUBERCULINIZACAO("Tuberculiniza��o"),
		WESTERN_BLOTTING("Western blotting");
		
		private String descricao;
		
		TipoTesteLaboratorial(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum AreaAtuacaoMedicoVeterinario {
		
		LABORATORIO_DE_DIAGNOSTICO("Laborat�rio de diagn�stico"),
		INSTITUICAO_DE_ENSINO_OU_PESQUISA("Institui��o de ensino ou pesquisa"),
		OUTRAS_INSTITUICOES_GOVERNAMENTAIS("Outras institui��es governamentais"),
		INICIATIVA_PRIVADA("Iniciativa privada"),
		OUTRA("Outra");
		
		private String descricao;
		
		AreaAtuacaoMedicoVeterinario(String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum TipoSaida {
	
		MALOTE("Malote"),
		EMAIL("Email"),
		SIZ("SIZ");
		
		private String descricao;
		
		TipoSaida (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum TipoUsuario {
		
		ADMINISTRATIVO_SVO("Administrativo SVO"),
		VETERINARIO_SVO("Veterin�rio SVO"),
		UNIVERSIDADE("Universidade"),
		LABORATORIO("Laborat�rio"),
		SERVICO_DE_INSPECAO("Servi�o de Inspe��o");
		
		private String descricao;
		
		TipoUsuario (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
	
	public enum Mes {
		
		JANEIRO(1, "Janeiro", "Jan"),
		FEVEREIRO(1, "Fevereiro", "Fev"),
		MARCO(3, "Mar�o", "Mar"),
		ABRIL(4, "Abril", "Abr"),
		MAIO(5, "Maio", "Mai"),
		JUNHO(6, "Junho", "Jun"),
		JULHO(7, "Julho", "Jul"),
		AGOSTO(8, "Agosto", "Ago"),
		SETEMBRO(9, "Setembro", "Set"),
		OUTUBRO(10, "Outubro", "Out"),
		NOVEMBRO(11, "Novembro", "Nov"),
		DEZEMBRO(12, "Dezembro", "Dez");
		
		private String descricao;
		
		private String abreviatura;
		
		private int ordem;
		
		Mes (int ordem, String descricao, String abreviatura) {
			this.ordem = ordem;
			this.descricao = descricao;
			this.abreviatura = abreviatura;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public String getAbreviatura() {
			return abreviatura;
		}

		public void setAbreviatura(String abreviatura) {
			this.abreviatura = abreviatura;
		}

		public int getOrdem() {
			return ordem;
		}

		public void setOrdem(int ordem) {
			this.ordem = ordem;
		}
		
	}
	
	public enum CondicaoMaterial {
		
		BOM(1, "Bom"),
		RUIM(2, "Ruim"),
		INSUFICIENTE(3, "Insuficiene");
		
		private String descricao;
		
		private int ordem;
		
		CondicaoMaterial (int ordem, String descricao) {
			this.ordem = ordem;
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public int getOrdem() {
			return ordem;
		}

		public void setOrdem(int ordem) {
			this.ordem = ordem;
		}
		
	}
	
	public enum AcompanhaNaoAcompanha {
		
		ACOMPANHA(1, "Acompanha a amostra"),
		NAO_ACOMPANHA(2, "N�o acompanha a amostra");
		
		private String descricao;
		
		private int ordem;
		
		AcompanhaNaoAcompanha (int ordem, String descricao) {
			this.ordem = ordem;
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public int getOrdem() {
			return ordem;
		}

		public void setOrdem(int ordem) {
			this.ordem = ordem;
		}
		
	}
	
	public enum TipoServicoDeInspecao {
		
		SIF("Servi�o de Inspe��o Federal"),
		SISE("Servi�o de Inspe��o Sanit�ria Estadual");
		
		private String descricao;
		
		TipoServicoDeInspecao (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum TipoDeMorteEmFrigorifico {
		
		ENCONTRADO_MORTO_NO_DESEMBARQUE("Encontrado morto no desembarque ao matadouro"),
		ENCONTRADO_MORTO_NO_MATADOURO("Encontrado morto nas instala��es do matadouro"),
		SUBMETIDO_A_ABATE_EMERGENCIAL("Submetido ao abate de emer�ncia");
		
		private String descricao;
		
		TipoDeMorteEmFrigorifico (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum MotivacaoParaAbateDeEmergenciaEmFrigorifico {
		
		CAQUEXIA_OU_DOENCA_CRONICA_DEPAUPERANTE("Caquexia ou doen�a cr�nica depauperante"),
		DECUBITO_ANIMAL_ALERTA("Dec�bito - animal alerta"),
		DECUBITO_ANIMAL_PROSTADO("Dec�bito - animal prostado"),
		DISTURBIOS_NERVOSOS("Dist�rbios nervosos"),
		FADIGA("Fadiga"),
		FRATURA("Fratura"),
		HEMORRAGIA("Hemorragia"),
		HIPERTEMIA("Hipertermia"),
		HIPORTEMIA("Hipotermia"),
		SIALORREIA("Sialorr�ia"),
		OUTRO("Outro");
		
		private String descricao;
		
		MotivacaoParaAbateDeEmergenciaEmFrigorifico (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel {
		
		ATAXIA_INCOORDENACAO("Ataxia/Incoordena��o"),
		CONVULSOES("Convuls�es"),
		ESPASMOS_MUSCULARES("Espasmos musculares"),
		MIDRIASE("Midr�ase"),
		MOVIMENTOS_DE_PEDALAGEM("Movimentos de pedalagem"),
		NISTAGNO("Nistagno"),
		OPISTOTONO("Opist�tono"),
		PARALISIA_DOS_MEMBROS_ANTERIORES("Paralisia dos membros anteriores"),
		PARALISIA_DOS_MEMBROS_POSTERIORES("Paralisia dos membros posteriores"),
		TREMORES("Tremores"),
		OUTRO("Outro");
		
		private String descricao;
		
		SinaisClinicosNervososEncefalopatoaEspongiformeTransmissivel (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum CategoriaAnimal {
		
		APTIDAO_LEITEIRA("Aptid�o leiteira"),
		CORTE_CONFINADO_SEMI("Corte (confinado/semi-confinado)"),
		CORTE_EXTENSIVO("Corte(extensivo)");
		
		private String descricao;
		
		CategoriaAnimal (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum ResultadoRelatorioDeEnsaio {
		
		BSE_NAO_DETECTADO("BSE n�o detectado"),
		DESCARTADO("Descartado"),
		POSITIVO_A_BSE("Positivo a BSE");
		
		private String descricao;
		
		ResultadoRelatorioDeEnsaio (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum TipoMotivoNotificacao {
		
		MORTES("Mortes"),
		SINTOMAS("Sintomas"),
		OUTRO("Outro");
		
		private String descricao;
		
		TipoMotivoNotificacao (String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

	}
	
	public enum StatusNotificacao {
		
		NOVO("Inclu�do");
		
		private String descricao;
		
		StatusNotificacao (String descricao){
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
	}
}
