package br.gov.mt.indea.sizapi.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.enums.Dominio.SimNao;
import br.gov.mt.indea.sizapi.enums.Dominio.TipoEstabelecimento;
import br.gov.mt.indea.sizapi.webservice.entity.Abatedouro;
import br.gov.mt.indea.sizapi.webservice.entity.Produtor;
import br.gov.mt.indea.sizapi.webservice.entity.Propriedade;
import br.gov.mt.indea.sizapi.webservice.entity.Recinto;
import br.gov.mt.indea.sizapi.webservice.entity.Servidor;

@Entity
@Table(name="visita_propriedade_rural")
public class VisitaPropriedadeRural extends BaseEntity<Long> implements Comparable<VisitaPropriedadeRural>{
	
	private static final long serialVersionUID = 4300644935073658135L;

	@Id
	@SequenceGenerator(name="visita_propriedade_rutal_seq", sequenceName="visita_propriedade_rutal_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="visita_propriedade_rutal_seq")
	private Long id;
	
	@JsonIgnore
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(length=17)
	private String numero;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_da_visita")
	private Calendar dataDaVisita;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
	
	@JsonIgnore
	@OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_proprietario")
    private Produtor proprietario;
	
	@Column(name="codigo_propriedade")
    private Long codigoPropriedade;
	
	@Column(name = "nome_propriedade")
    private String nomePropriedade;
	
	@JsonIgnore
	@OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_recinto")
	private Recinto recinto;
	
	@JsonIgnore
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinColumn(name="id_abatedouro")
	private Abatedouro abatedouro;
	
	@JsonIgnore
	@OneToMany(mappedBy="visitaPropriedadeRural", cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	private List<ChavePrincipalVisitaPropriedadeRural> listChavePrincipalVisitaPropriedadeRural = new ArrayList<ChavePrincipalVisitaPropriedadeRural>();
	
	@JsonIgnore
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="visita_prop_rural_servidores", joinColumns=@JoinColumn(name="id_visita_propriedade_rural"), inverseJoinColumns=@JoinColumn(name="id_pessoa"))
	private List<Servidor> listServidores = new ArrayList<Servidor>();
	
	@Column(length=4000)
	private String resumo;
	
//	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
//	@JoinColumn(name="id_detalhe_comunicacao_aie")
//	private DetalheComunicacaoAIE detalheComunicacaoAIE;
//	
//	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
//	@JoinColumn(name="id_registro_entrada_lasa")
//	private RegistroEntradaLASA registroEntradaLASA;
//	
//	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
//	@JoinColumn(name="id_detalhe_form_notifica")
//	private DetalheFormNotifica detalheFormNotifica;
//	
//	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, orphanRemoval=false, fetch=FetchType.LAZY)
//	@JoinColumn(name="id_lote_enf_abat_frig")
//	private LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico;
		
	public boolean possuiMotivoPrincipal(){
		if (this.listChavePrincipalVisitaPropriedadeRural != null){
			for (ChavePrincipalVisitaPropriedadeRural chave : listChavePrincipalVisitaPropriedadeRural) {
				if (chave.getMotivoPrincipal().equals(SimNao.SIM))
					return true;
			}
		}
		
		return false;
	}
	
	public TipoEstabelecimento getTipoEstabelecimento(){
		if (this.propriedade != null)
			return TipoEstabelecimento.PROPRIEDADE;
		else if (this.recinto != null)
			return TipoEstabelecimento.RECINTO;
		else
			return TipoEstabelecimento.ABATEDOURO;
	}
	
	public String getNomeEstabelecimento(){
		if (this.propriedade != null)
			return propriedade.getNome();
		else if (this.recinto != null)
			return recinto.getNome();
		else
			return abatedouro.getNome();
	}
	
	public boolean isEstabelecimentoPropriedade(){
		return this.propriedade != null;
	}
	
	public boolean isEstabelecimentoRecinto(){
		return this.recinto != null;
	}
	
	public boolean isEstabelecimentoAbatedouro(){
		return this.abatedouro != null;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public Calendar getDataDaVisita() {
		return dataDaVisita;
	}

	public void setDataDaVisita(Calendar dataDaVisita) {
		this.dataDaVisita = dataDaVisita;
	}

	public List<ChavePrincipalVisitaPropriedadeRural> getListChavePrincipalVisitaPropriedadeRural() {
		return listChavePrincipalVisitaPropriedadeRural;
	}

	public void setListChavePrincipalVisitaPropriedadeRural(
			List<ChavePrincipalVisitaPropriedadeRural> listChavePrincipalVisitaPropriedadeRural) {
		this.listChavePrincipalVisitaPropriedadeRural = listChavePrincipalVisitaPropriedadeRural;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

//	public Servidor getServidor() {
//		return servidor;
//	}
//
//	public void setServidor(Servidor servidor) {
//		this.servidor = servidor;
//	}

	public Recinto getRecinto() {
		return recinto;
	}

	public void setRecinto(Recinto recinto) {
		this.recinto = recinto;
	}

	public List<Servidor> getListServidores() {
		return listServidores;
	}

	public void setListServidores(List<Servidor> listServidores) {
		this.listServidores = listServidores;
	}

	public Abatedouro getAbatedouro() {
		return abatedouro;
	}

	public void setAbatedouro(Abatedouro abatedouro) {
		this.abatedouro = abatedouro;
	}

//	public DetalheComunicacaoAIE getDetalheComunicacaoAIE() {
//		return detalheComunicacaoAIE;
//	}
//
//	public void setDetalheComunicacaoAIE(DetalheComunicacaoAIE detalheComunicacaoAIE) {
//		this.detalheComunicacaoAIE = detalheComunicacaoAIE;
//	}
//
//	public RegistroEntradaLASA getRegistroEntradaLASA() {
//		return registroEntradaLASA;
//	}
//
//	public void setRegistroEntradaLASA(RegistroEntradaLASA registroEntradaLASA) {
//		this.registroEntradaLASA = registroEntradaLASA;
//	}
//
//	public DetalheFormNotifica getDetalheFormNotifica() {
//		return detalheFormNotifica;
//	}
//
//	public void setDetalheFormNotifica(DetalheFormNotifica detalheFormNotifica) {
//		this.detalheFormNotifica = detalheFormNotifica;
//	}
//
//	public LoteEnfermidadeAbatedouroFrigorifico getLoteEnfermidadeAbatedouroFrigorifico() {
//		return loteEnfermidadeAbatedouroFrigorifico;
//	}
//
//	public void setLoteEnfermidadeAbatedouroFrigorifico(
//			LoteEnfermidadeAbatedouroFrigorifico loteEnfermidadeAbatedouroFrigorifico) {
//		this.loteEnfermidadeAbatedouroFrigorifico = loteEnfermidadeAbatedouroFrigorifico;
//	}

	public Produtor getProprietario() {
		return proprietario;
	}

	public void setProprietario(Produtor proprietario) {
		this.proprietario = proprietario;
	}

	public Long getCodigoPropriedade() {
		return codigoPropriedade;
	}

	public void setCodigoPropriedade(Long codigoPropriedade) {
		this.codigoPropriedade = codigoPropriedade;
	}

	public String getNomePropriedade() {
		return nomePropriedade;
	}

	public void setNomePropriedade(String nomePropriedade) {
		this.nomePropriedade = nomePropriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitaPropriedadeRural other = (VisitaPropriedadeRural) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(VisitaPropriedadeRural o) {
		if (this.getDataDaVisita().after(o.getDataDaVisita()))
			return -1;
		else if (this.getDataDaVisita().before(o.getDataDaVisita()))
			return 1;
		else
			return 0;
	}

}
