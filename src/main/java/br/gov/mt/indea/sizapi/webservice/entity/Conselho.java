package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
public class Conselho extends BaseEntity<String> implements Cloneable{

	private static final long serialVersionUID = -1759623691051845140L;

	@Id
    @SequenceGenerator(name = "conselho_seq", sequenceName = "conselho_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conselho_seq")
	private Long idTable;
	
	@Column(name = "identificacao_conselho")
	private String id;
	
	private String nome;
	
	@Column(name = "codigo_pga")
	private Integer codigoPga;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigoPga() {
		return codigoPga;
	}

	public void setCodigoPga(Integer codigoPga) {
		this.codigoPga = codigoPga;
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
	
	protected Object clone() throws CloneNotSupportedException {
		Conselho cloned = (Conselho) super.clone();
		
		cloned.setIdTable(null);
		
		return cloned;
	}
	
}