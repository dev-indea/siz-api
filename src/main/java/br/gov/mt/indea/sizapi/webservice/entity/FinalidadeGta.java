package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name="finalidade_gta")
public class FinalidadeGta extends BaseEntity<Long> {

    private static final long serialVersionUID = -8755693590006154020L;

	@Id
    @SequenceGenerator(name = "finalidade_gta_seq", sequenceName = "finalidade_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "finalidade_gta_seq")
	private Long id;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "fgt_atualiza_saldo_destino")
    private String atualizaSaldoDestino;
    
    @Column(name = "fgt_de_propriedade")
    private String dePropriedade;
    
    @Column(name = "fgt_de_aglomeracao")
    private String deAglomeracao;
    
    @Column(name = "fgt_de_estabelecimento")
    private String deEstabelecimento;
    
    @Column(name = "fgt_para_propriedade")
    private String paraPropriedade;
    
    @Column(name = "fgt_para_aglomeracao")
    private String paraAglomeracao;
    
    @Column(name = "fgt_para_estabelecimento")
    private String paraEstabelecimento;
    
    @Column(name = "codigo_pga")
    private String codigoPga;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAtualizaSaldoDestino() {
        return atualizaSaldoDestino;
    }

    public void setAtualizaSaldoDestino(String atualizaSaldoDestino) {
        this.atualizaSaldoDestino = atualizaSaldoDestino;
    }

    public String getDePropriedade() {
        return dePropriedade;
    }

    public void setDePropriedade(String dePropriedade) {
        this.dePropriedade = dePropriedade;
    }

    public String getDeAglomeracao() {
        return deAglomeracao;
    }

    public void setDeAglomeracao(String deAglomeracao) {
        this.deAglomeracao = deAglomeracao;
    }

    public String getDeEstabelecimento() {
        return deEstabelecimento;
    }

    public void setDeEstabelecimento(String deEstabelecimento) {
        this.deEstabelecimento = deEstabelecimento;
    }

    public String getParaPropriedade() {
        return paraPropriedade;
    }

    public void setParaPropriedade(String paraPropriedade) {
        this.paraPropriedade = paraPropriedade;
    }

    public String getParaAglomeracao() {
        return paraAglomeracao;
    }

    public void setParaAglomeracao(String paraAglomeracao) {
        this.paraAglomeracao = paraAglomeracao;
    }

    public String getParaEstabelecimento() {
        return paraEstabelecimento;
    }

    public void setParaEstabelecimento(String paraEstabelecimento) {
        this.paraEstabelecimento = paraEstabelecimento;
    }

    public String getCodigoPga() {
        return codigoPga;
    }

    public void setCodigoPga(String codigoPga) {
        this.codigoPga = codigoPga;
    }

}