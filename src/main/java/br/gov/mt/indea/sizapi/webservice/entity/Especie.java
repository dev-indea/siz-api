package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
public class Especie extends BaseEntity<Long> {

	private static final long serialVersionUID = -8466642547334193944L;

	@Id
    @SequenceGenerator(name = "especie_seq", sequenceName = "especie_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "especie_seq")
	private Long id;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_grupo_especie")
    private GrupoEspecie grupoEspecie;
    
    @Column(name = "nome")
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrupoEspecie getGrupoEspecie() {
        return grupoEspecie;
    }

    public void setGrupoEspecie(GrupoEspecie grupoEspecie) {
        this.grupoEspecie = grupoEspecie;
    }

    public String getNome() {
        return nome;
    }

    public void setEsp_nome(String nome) {
        this.nome = nome;
    }
    
}