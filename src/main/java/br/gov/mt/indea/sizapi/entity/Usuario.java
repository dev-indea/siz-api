package br.gov.mt.indea.sizapi.entity;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.gov.mt.indea.sizapi.entity.seguranca.Grupo;
import br.gov.mt.indea.sizapi.entity.seguranca.Permissao;
import br.gov.mt.indea.sizapi.enums.Dominio.AtivoInativo;
import br.gov.mt.indea.sizapi.enums.Dominio.TipoUsuario;
import br.gov.mt.indea.sizapi.webservice.entity.ULE;
import br.gov.mt.indea.sizapi.webservice.entity.Veterinario;

@Entity
public class Usuario extends BaseEntity<String> implements Serializable{


	private static final long serialVersionUID = 3670487123137907453L;


	@Id
	private String id;


	private String password;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@Column(length=300)
	private String nome;
	
	private String cpf;
	
	private String email;
	
	@Column(name="telefone_fixo")
	private String telefoneFixo;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private AtivoInativo status = AtivoInativo.ATIVO;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_ultimo_login")
	private Date ultimoLogin = new Date();
	
	@JsonIgnore
	@ManyToMany(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinTable(name="usuario_grupos", joinColumns=@JoinColumn(name="id_usuario", unique=false), inverseJoinColumns=@JoinColumn(name="id_grupo", unique=false))
	private List<Grupo> listaGrupo = new ArrayList<Grupo>();
	
	@JsonIgnore
	@ManyToMany(cascade={CascadeType.DETACH, CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinTable(name="usuario_permissoes", joinColumns=@JoinColumn(name="id_usuario", unique=false), inverseJoinColumns=@JoinColumn(name="id_permissao", unique=false))
	private List<Permissao> listaPermissao = new ArrayList<Permissao>();
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_usuario")
    private TipoUsuario tipoUsuario;
	
	@JsonIgnore
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_unidade")
    private ULE unidade;
	
	@JsonIgnore
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_laboratorio")
    private Laboratorio laboratorio;
	
	@JsonIgnore
	@OneToMany(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario_servico_de_inspecao")
	private List<UsuarioServicoDeInspecao> listaServicoDeInspecao;
	
	@JsonIgnore
	@ManyToOne(cascade={CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, fetch=FetchType.LAZY)
    @JoinColumn(name = "id_universidade")
    private Universidade universidade;
	
	@JsonIgnore
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH}, fetch=FetchType.LAZY)
	@JoinColumn(name="id_veterinario")
	private Veterinario veterinarioRemetente;
	
	public boolean isUsuarioAtivo(){
		if (this.status != null)
			return this.status.equals(AtivoInativo.ATIVO);
		return false;
	}
	
	public boolean isUsuarioExpirado(){
		return this.status.equals(AtivoInativo.EXPIRADO);
	}
	
	public String getPrimeiroNome(){
		if (nome != null){
			String[] nomes = nome.split(" ");
			return nomes[0];
		}
			
		return null;
	}
	
	public boolean isUsuarioVeterinarioSVO(){
		if (this.getTipoUsuario() == null)
			return false;
		if (this.getTipoUsuario().equals(TipoUsuario.VETERINARIO_SVO))
			return true;
		return false;
	}
	
	public boolean isUsuarioUniversidade(){
		if (this.getTipoUsuario() == null)
			return false;
		if (this.getTipoUsuario().equals(TipoUsuario.UNIVERSIDADE))
			return true;
		return false;
	}
	
	public boolean isUsuarioLaboratorio(){
		if (this.getTipoUsuario() == null)
			return false;
		if (this.getTipoUsuario().equals(TipoUsuario.LABORATORIO))
			return true;
		return false;
	}
	
	public boolean isUsuarioServicoDeInspecao(){
		if (this.getTipoUsuario() == null)
			return false;
		if (this.getTipoUsuario().equals(TipoUsuario.SERVICO_DE_INSPECAO))
			return true;
		return false;
	}
	
	public Municipio getMunicipio(){
		if (this.unidade != null)
			return this.unidade.getMunicipio();
		
		if (this.laboratorio != null)
			return this.getLaboratorio().getEndereco().getMunicipio();
		
		if (this.getUniversidade() != null)
			return this.getUniversidade().getEndereco().getMunicipio();
		
		if (this.getServicoDeInspecaoEmUso() != null) {
			return null;
		}

		return null;
	}
	
	public ServicoDeInspecao getServicoDeInspecaoEmUso() {
		if (listaServicoDeInspecao != null)
			for (UsuarioServicoDeInspecao usuarioServicoDeInspecao : listaServicoDeInspecao) {
				if (usuarioServicoDeInspecao.isEmUso())
					return usuarioServicoDeInspecao.getServicoDeInspecao();
			}
		
		return null;
	}
	
	public boolean contem(ServicoDeInspecao servicoDeInspecao) {
		for (UsuarioServicoDeInspecao usuarioServicoDeInspecao : listaServicoDeInspecao) {
			if (usuarioServicoDeInspecao.getServicoDeInspecao().equals(servicoDeInspecao))
				return true;
		}
		
		return false;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefoneFixo() {
		return telefoneFixo;
	}

	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}

	public AtivoInativo getStatus() {
		return status;
	}

	public void setStatus(AtivoInativo status) {
		this.status = status;
	}

	public Date getUltimoLogin() {
		return ultimoLogin;
	}

	public void setUltimoLogin(Date ultimoLogin) {
		this.ultimoLogin = ultimoLogin;
	}

	public List<Grupo> getListaGrupo() {
		return listaGrupo;
	}

	public void setListaGrupo(List<Grupo> listaGrupo) {
		this.listaGrupo = listaGrupo;
	}

	public List<Permissao> getListaPermissao() {
		if (this.listaPermissao == null)
			this.listaPermissao = new ArrayList<>();
		return listaPermissao;
	}

	public void setListaPermissao(List<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}

	public ULE getUnidade() {
		return unidade;
	}

	public void setUnidade(ULE unidade) {
		this.unidade = unidade;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public Universidade getUniversidade() {
		return universidade;
	}

	public void setUniversidade(Universidade universidade) {
		this.universidade = universidade;
	}

	public List<UsuarioServicoDeInspecao> getListaServicoDeInspecao() {
		return listaServicoDeInspecao;
	}

	public void setListaServicoDeInspecao(List<UsuarioServicoDeInspecao> listaServicoDeInspecao) {
		this.listaServicoDeInspecao = listaServicoDeInspecao;
	}

	public Veterinario getVeterinarioRemetente() {
		return veterinarioRemetente;
	}

	public void setVeterinarioRemetente(Veterinario veterinarioRemetente) {
		this.veterinarioRemetente = veterinarioRemetente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", password=" + password + ", dataCadastro=" + dataCadastro + ", nome=" + nome
				+ ", cpf=" + cpf + ", email=" + email + ", telefoneFixo=" + telefoneFixo + ", status=" + status
				+ ", ultimoLogin=" + ultimoLogin + "]";
	}
	
}