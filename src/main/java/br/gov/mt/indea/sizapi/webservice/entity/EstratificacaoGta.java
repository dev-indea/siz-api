package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
public class EstratificacaoGta extends BaseEntity<Long>{

	private static final long serialVersionUID = 1008130316298237689L;

	@Id
    @SequenceGenerator(name = "estratificacao_gta_seq", sequenceName = "estratificacao_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "estratificacao_gta_seq")
    private Long id;
    
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="id_estratificacao")
	private Estratificacao estratificacao;
    
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="id_tipo_saldo")
	private TipoSaldo tipoSaldo;
    
	private Integer qntEnviada;
    
	private Integer qntRecebida;
    
	private String itemValido;
	
	@ManyToOne
	@JoinColumn(name="id_gta")
	private Gta gta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estratificacao getEstratificacao() {
        return estratificacao;
    }

    public void setEstratificacao(Estratificacao estratificacao) {
        this.estratificacao = estratificacao;
    }

    public TipoSaldo getTipoSaldo() {
        return tipoSaldo;
    }

    public void setTipoSaldo(TipoSaldo tipoSaldo) {
        this.tipoSaldo = tipoSaldo;
    }

    public Integer getQntEnviada() {
        return qntEnviada;
    }

    public void setQntEnviada(Integer qntEnviada) {
        this.qntEnviada = qntEnviada;
    }

    public Integer getQntRecebida() {
        return qntRecebida;
    }

    public void setQntRecebida(Integer qntRecebida) {
        this.qntRecebida = qntRecebida;
    }

    public String getItemValido() {
        return itemValido;
    }

    public void setItemValido(String itemValido) {
        this.itemValido = itemValido;
    }

	public Gta getGta() {
		return gta;
	}

	public void setGta(Gta gta) {
		this.gta = gta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstratificacaoGta other = (EstratificacaoGta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
