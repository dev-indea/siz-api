package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity(name = "tipo_gta")
public class TipoGta extends BaseEntity<String> {
    
    private static final long serialVersionUID = 8719558119625350837L;

	@Id
    @SequenceGenerator(name = "tipo_gta_seq", sequenceName = "tipo_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tipo_gta_seq")
    private Long idTable;
	
	@Column(name = "identificador_tipo_gta")
    private String id;
    
    @Column(name = "nome")
    private String nome;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}
	
}