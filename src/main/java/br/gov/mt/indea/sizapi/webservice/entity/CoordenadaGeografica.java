package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.mt.indea.sizapi.entity.BaseEntity;

@Entity
@Table(name = "coordenadageografica")
public class CoordenadaGeografica extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = -2430515477730393843L;

	@Id
    @SequenceGenerator(name = "coordenada_geografica_seq", sequenceName = "coordenada_geografica_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "coordenada_geografica_seq")
	private Long id;
    
    @Column(name = "orientacao_longitude")
	private String orientacaoLongitude;
    
    @Column(name = "pessoa_id")
	private Integer pessoa_id;
    
    @Column(name = "orientacao_latitude")
	private String orientacaoLatitude;
    
    @Column(name = "latitude_grau")
	private Integer latGrau;
    
    @Column(name = "latitude_minuto")
	private Integer latMin;

    @Column(name = "latitude_segundo")
	private Float latSeg;
    
    @Column(name = "longitude_grau")
	private Integer longGrau;
   
    @Column(name = "longitude_minuto")
	private Integer longMin;
     
    @Column(name = "longitude_segundo")
	private Float longSeg;
    
    public boolean isNull(){
    	if (orientacaoLatitude == null || latGrau == null || latMin == null || latSeg == null ||
    	    orientacaoLongitude == null || longGrau == null || longMin == null || longSeg == null)
    		return true;
    	return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrientacaoLongitude() {
        return orientacaoLongitude;
    }

    public void setOrientacaoLongitude(String orientacaoLongitude) {
        this.orientacaoLongitude = orientacaoLongitude;
    }

    public String getOrientacaoLatitude() {
        return orientacaoLatitude;
    }

    public void setOrientacaoLatitude(String orientacaoLatitude) {
        this.orientacaoLatitude = orientacaoLatitude;
    }

    public Integer getLatGrau() {
        return latGrau;
    }

    public void setLatGrau(Integer latGrau) {
        this.latGrau = latGrau;
    }

    public Integer getLatMin() {
        return latMin;
    }

    public void setLatMin(Integer latMin) {
        this.latMin = latMin;
    }

    public Float getLatSeg() {
        return latSeg;
    }

    public void setLatSeg(Float latSeg) {
        this.latSeg = latSeg;
    }

    public Integer getLongGrau() {
        return longGrau;
    }

    public void setLongGrau(Integer longGrau) {
        this.longGrau = longGrau;
    }

    public Integer getLongMin() {
        return longMin;
    }

    public void setLongMin(Integer longMin) {
        this.longMin = longMin;
    }

    public Float getLongSeg() {
        return longSeg;
    }

    public void setLongSeg(Float longSeg) {
        this.longSeg = longSeg;
    }

    public Integer getPessoa_id() {
        return pessoa_id;
    }

    public void setPessoa_id(Integer pessoa_id) {
        this.pessoa_id = pessoa_id;
    }

    protected Object clone() throws CloneNotSupportedException{
		CoordenadaGeografica cloned = (CoordenadaGeografica) super.clone();
		
		cloned.setId(null);
		
		return cloned;
	}
    
}