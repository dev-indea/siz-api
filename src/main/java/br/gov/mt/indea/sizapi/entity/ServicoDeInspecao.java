package br.gov.mt.indea.sizapi.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.gov.mt.indea.sizapi.enums.Dominio.TipoServicoDeInspecao;
import br.gov.mt.indea.sizapi.webservice.entity.Abatedouro;

@Entity
@Table(name="servico_de_inspecao")
public class ServicoDeInspecao extends BaseEntity<Long> implements Cloneable {

	private static final long serialVersionUID = -7558247653822818449L;
	
	@Id
	@SequenceGenerator(name="servico_inspecao_seq", sequenceName="servico_inspecao_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="servico_inspecao_seq")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	private Calendar dataCadastro = Calendar.getInstance();
	
	@Column(unique=true)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_servico_de_inspecao")
    private TipoServicoDeInspecao tipoServicoDeInspecao;

	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval=true, fetch=FetchType.EAGER)
	@JoinColumn(name="id_abatedouro")
	private Abatedouro abatedouro;
	
	@OneToMany(mappedBy = "servicoDeInspecao", fetch = FetchType.EAGER)
	private List<NumeroFormularioDeColheitaTroncoEncefalico> listaNumeroFormularioDeColheitaTroncoEncefalico;
	
	public String porExtenso(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.tipoServicoDeInspecao.name())
		  .append(" - ")
		  .append(this.numero)
		  .append(" - ")
		  .append(this.abatedouro.getNome());
		
		return sb.toString();
	}
	
	public String porExtensoTipoENumero(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.tipoServicoDeInspecao.name())
		  .append("-")
		  .append(this.numero);
		  
		
		return sb.toString();
	}
	
	public Long getUltimoNumeroFormularioDeColheitaTroncoEncefalico() {
		NumeroFormularioDeColheitaTroncoEncefalico numero = this.getNumeroFormularioDeColheitaTroncoEncefalicoMaisRecente();
		
		if (numero != null)
			return numero.getUltimoNumero();
		
		return null;
	}
	
	public NumeroFormularioDeColheitaTroncoEncefalico getNumeroFormularioDeColheitaTroncoEncefalicoMaisRecente() {
		NumeroFormularioDeColheitaTroncoEncefalico numero = null;
		
		if (this.listaNumeroFormularioDeColheitaTroncoEncefalico != null) {
			for (NumeroFormularioDeColheitaTroncoEncefalico numeroFormularioDeColheitaTroncoEncefalico : listaNumeroFormularioDeColheitaTroncoEncefalico) {
				if (numero == null)
					numero = numeroFormularioDeColheitaTroncoEncefalico;
				else 
					if (numeroFormularioDeColheitaTroncoEncefalico.getAno() > numero.getAno())
						numero = numeroFormularioDeColheitaTroncoEncefalico;
			}
		}
		
		return numero;
	}
	
	public Abatedouro getAbatedouro() {
		return abatedouro;
	}

	public void setAbatedouro(Abatedouro abatedouro) {
		this.abatedouro = abatedouro;
	}

	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public TipoServicoDeInspecao getTipoServicoDeInspecao() {
		return tipoServicoDeInspecao;
	}

	public void setTipoServicoDeInspecao(TipoServicoDeInspecao tipoServicoDeInspecao) {
		this.tipoServicoDeInspecao = tipoServicoDeInspecao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public List<NumeroFormularioDeColheitaTroncoEncefalico> getListaNumeroFormularioDeColheitaTroncoEncefalico() {
		return listaNumeroFormularioDeColheitaTroncoEncefalico;
	}

	public void setListaNumeroFormularioDeColheitaTroncoEncefalico(
			List<NumeroFormularioDeColheitaTroncoEncefalico> listaNumeroFormularioDeColheitaTroncoEncefalico) {
		this.listaNumeroFormularioDeColheitaTroncoEncefalico = listaNumeroFormularioDeColheitaTroncoEncefalico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((tipoServicoDeInspecao == null) ? 0 : tipoServicoDeInspecao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicoDeInspecao other = (ServicoDeInspecao) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (tipoServicoDeInspecao != other.tipoServicoDeInspecao)
			return false;
		return true;
	}
}
