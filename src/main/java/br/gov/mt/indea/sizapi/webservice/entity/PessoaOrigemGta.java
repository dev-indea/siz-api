package br.gov.mt.indea.sizapi.webservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.gov.mt.indea.sizapi.entity.BaseEntity;
import br.gov.mt.indea.sizapi.entity.Municipio;
import br.gov.mt.indea.sizapi.entity.Pessoa;
import br.gov.mt.indea.sizapi.entity.UF;

@Entity(name = "pessoa_origem_gta")
public class PessoaOrigemGta extends BaseEntity<Long>  {

    private static final long serialVersionUID = 5919430305441605390L;

	@Id
    @SequenceGenerator(name = "pessoa_origem_gta_seq", sequenceName = "pessoa_origem_gta_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_origem_gta_seq")
    private Long id;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_produtor")
    private Pessoa produtor;
    
    @OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_propriedade")
	private Propriedade propriedade;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "id_exploracao_propriedade")
    private Exploracao exploracao;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_municipio")
    private Municipio municipio;
    
    @Column(name = "cnpj_cpf")
    private String cnpjCpf;
    
    @Column(name = "codigo_aglomeracao")
    private String codigoAglomeracao;
    
    public boolean isPessoaOrigemDeOutroEstado() {
    	if (this.propriedade == null)
    		return false;
    	else if (this.propriedade.getMunicipio() == null)
    		return false;
    	else if (this.propriedade.getMunicipio().getUf().equals(UF.MATO_GROSSO))
    		return false;
    	else 
    		return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getProdutor() {
        return produtor;
    }

    public void setProdutor(Pessoa produtor) {
        this.produtor = produtor;
    }

    public Exploracao getExploracao() {
        return exploracao;
    }

    public void setExploracao(Exploracao exploracao) {
        this.exploracao = exploracao;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getCodigoAglomeracao() {
        return codigoAglomeracao;
    }

    public void setCodigoAglomeracao(String codigoAglomeracao) {
        this.codigoAglomeracao = codigoAglomeracao;
    }

	public Propriedade getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(Propriedade propriedade) {
		this.propriedade = propriedade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((propriedade == null) ? 0 : propriedade.hashCode());
		result = prime * result + ((exploracao == null) ? 0 : exploracao.hashCode());
		result = prime * result + ((produtor == null) ? 0 : produtor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaOrigemGta other = (PessoaOrigemGta) obj;
		if (propriedade == null) {
			if (other.propriedade != null)
				return false;
		} else if (!propriedade.equals(other.propriedade))
			return false;
		if (exploracao == null) {
			if (other.exploracao != null)
				return false;
		} else if (!exploracao.equals(other.exploracao))
			return false;
		if (produtor.getCodigo() == null) {
			if (other.produtor.getCodigo() != null)
				return false;
		} else if (!produtor.getCodigo().equals(other.produtor.getCodigo()))
			return false;
		return true;
	}

}