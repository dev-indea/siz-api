package br.gov.mt.indea.sizapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="municipio")
public class Municipio extends BaseEntity<Long> {
	
	private static final long serialVersionUID = 1006539915736512439L;
	
	@Id
	@SequenceGenerator(name="municipio_seq", sequenceName="municipio_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="municipio_seq")
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_uf", nullable=false)
	private UF uf;
	
	@Column(length=140)
	private String nome;
	
	@Column(name="codg_ibge", length=5)
	private String codgIBGE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UF getUf() {
		return uf;
	}

	public void setUf(UF uf) {
		this.uf = uf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodgIBGE() {
		return codgIBGE;
	}

	public void setCodgIBGE(String codgIBGE) {
		this.codgIBGE = codgIBGE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
