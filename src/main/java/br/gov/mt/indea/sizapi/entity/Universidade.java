package br.gov.mt.indea.sizapi.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("UNIVERSIDADE")
public class Universidade extends Pessoa implements Cloneable {

	private static final long serialVersionUID = -7571727456856230547L;
	
}
